Time for word graphs: 1804,86300 seconds
Time for n-gram graphs: 0,00000 seconds
Time for files of word graphs: 13012,44900 seconds
Time for files of n-gram graphs: 0,00000 seconds
Time for files of bagOfWords: 0,00000 seconds

Results WordGraphs
==========


Correctly Classified Instances        1644               82.2    %
Incorrectly Classified Instances       356               17.8    %
Kappa statistic                          0.644 
Mean absolute error                      0.2058
Root mean squared error                  0.3534
Relative absolute error                 41.1552 %
Root relative squared error             70.6868 %
Coverage of cases (0.95 level)          97.1    %
Mean rel. region size (0.95 level)      73.1    %
Total Number of Instances             2000     

Time for classifier of word graphs: 0,08300 seconds
Time for classifier of n-gram graphs: 0,00000 seconds
Time for classifier of bagOfWords: 0,00000 seconds
Time for evaluating classifier of word graphs: 0,04700 seconds
Time for evaluating classifier of n-gram graphs: 0,00000 seconds
Time for evaluating classifier of bagOfWords: 0,00000 seconds

Execution time is 14818,51300 seconds
Time for word graphs: 1835,12400 seconds
Time for files of word graphs: 13568,76100 seconds

Results WordGraphs
==========


Correctly Classified Instances        1633               81.65   %
Incorrectly Classified Instances       367               18.35   %
Kappa statistic                          0.633 
Mean absolute error                      0.2071
Root mean squared error                  0.3702
Relative absolute error                 41.4157 %
Root relative squared error             74.0498 %
Coverage of cases (0.95 level)          95.1    %
Mean rel. region size (0.95 level)      70.525  %
Total Number of Instances             2000     

Time for classifier of word graphs: 0,09400 seconds
Time for evaluating classifier of word graphs: 0,04700 seconds

Execution time is 15404,88400 seconds
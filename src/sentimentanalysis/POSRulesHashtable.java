package sentimentanalysis;

import java.util.List;
import java.util.Properties;
import java.util.Hashtable;
import java.util.Iterator;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class POSRulesHashtable{

	private int noOfTrainRuleReviews;

	private String posTrainRuleFilepath;
	private String negTrainRuleFilepath;

	private MaxentTagger tagger;
	private Hashtable <String, POSRulesHashtableValues> POSRulesHashtable;
	private int thresholdNoOfAppearances;
	private double thresholdPercentage;

	private SentiWordNet SentiWordNet;

	private ArrayList<String> posTrainRuleFilenames;
	private ArrayList<String> negTrainRuleFilenames;

	private double posRate;
	private boolean shuffle;
	private long seed; 

	private int minPosRating;
	private int maxPosRating;
	private int minNegRating;
	private int maxNegRating;

	private File resultsDirectory;


	public static void main(String[] args) 
			throws IOException, ClassNotFoundException {

		if (args.length != 1) return;

		InputStream reader = new FileInputStream(args[0]);
		Properties properties = new Properties();
		if (args[0].contains(".xml"))
			properties.loadFromXML(reader);
		else
			properties.load(reader);
		long start = System.currentTimeMillis();

		System.out.println("Creating files for setting POS rules.\n");

		POSRulesHashtable object = new POSRulesHashtable();

		object.setParameters(properties, true);
		object.createResultsDirectory();
		object.findTrainRuleFilenames();
		object.createPOSRulesHashtable();
		long end = System.currentTimeMillis();
		NumberFormat formatter = new DecimalFormat("#0.00000");
		System.out.print("\nExecution time is " + formatter.format(
				(end - start) / 1000d) + " seconds");
	}

	public POSRulesHashtable(){ }

	public POSRulesHashtable(String taggerFilepath, String 
			POSRulesHashtableFilepath, String SentiWordNetFilepath,
			int thresholdNoOfAppearances, double thresholdPercentage) 
					throws ClassNotFoundException, IOException{

		setTagger(new MaxentTagger(taggerFilepath));
		setThresholdNoOfAppearances(thresholdNoOfAppearances);
		setThresholdPercentage(thresholdPercentage);
		setSentiWordNet(new SentiWordNet
				(SentiWordNetFilepath));
		setPOSRulesHashtable(loadPOSRulesHashtable
				(POSRulesHashtableFilepath));
	}

	public void setParameters(Properties properties, boolean 
			onlyThisStage) throws ClassNotFoundException, IOException {

		System.out.println("Setting parameters...");

		setNoOfTrainRuleReviews(Integer.parseInt(properties.
				getProperty("noOfTrainRuleReviews")));
		setMinPosRating(Integer.parseInt(properties.
				getProperty("minPositiveRating")));
		setMaxPosRating(Integer.parseInt(properties.
				getProperty("maxPositiveRating")));
		setMinNegRating(Integer.parseInt(properties.
				getProperty("minNegativeRating")));
		setMaxNegRating(Integer.parseInt(properties.
				getProperty("maxNegativeRating")));
		setPosRate(Double.parseDouble(properties.
				getProperty("positiveRate"))/100);

		if (properties.getProperty("seed") != null) 
			setSeed(Long.parseLong(properties.getProperty("seed")));
		else setSeed(-1);
		setShuffle(Boolean.parseBoolean(properties.
				getProperty("shuffle")));

		setPosTrainRuleFilepath(properties.
				getProperty("positiveTrainRuleFilepath"));
		setNegTrainRuleFilepath(properties.
				getProperty("negativeTrainRuleFilepath"));
		setTagger(new MaxentTagger(properties.
				getProperty("taggerFilepath")));
		setSentiWordNet(new SentiWordNet(properties.
				getProperty("SentiWordNetFilepath")));
		setThresholdNoOfAppearances(Integer.parseInt(properties.
				getProperty("thresholdNoOfAppearances")));
		setThresholdPercentage(Integer.parseInt(properties.
				getProperty("thresholdPercentage")));
	}

	public void createResultsDirectory() {
		System.out.println("Creating directory for results...");

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);

		String results;

		results = getNoOfTrainRuleReviews() + "_";
		results = getThresholdNoOfAppearances() + "_";
		results+= getThresholdPercentage() +"PER";
		results+= "_" + day + "" + month + "" + year;
		results+= "_" + hours + "." + minutes + "." + seconds;

		setResultsDirectory(new File(results));
		getResultsDirectory().mkdir();
	}

	public void findTrainRuleFilenames() throws IOException {
		System.out.println("Selecting reviews for creating POS rules...");

		FilenamePattern filePattern = new FilenamePattern
				(getMinPosRating(), getMaxPosRating());
		setPosTrainRuleFilenames(filePattern.findFilenames(
				getPosTrainRuleFilepath(), 
				(int) (getNoOfTrainRuleReviews() * posRate), 
				isShuffle(), getSeed()));

		filePattern = new FilenamePattern(getMinNegRating(),
				getMaxNegRating());
		setNegTrainRuleFilenames(filePattern.findFilenames(
				getNegTrainRuleFilepath(), 
				(int) (getNoOfTrainRuleReviews() * (1 - posRate)),
				isShuffle(), getSeed()));

		writeFilenames(getPosTrainRuleFilenames(),
				"FilenamesForTrainRule.txt");
		writeFilenames(getNegTrainRuleFilenames(),
				"FilenamesForTrainRule.txt");
	}

	public void createPOSRulesHashtable() throws ClassNotFoundException, 
	IOException{

		MaxentTagger tagger = getTagger();
		Hashtable<String, POSRulesHashtableValues> 
		SentimentRulesMap = new Hashtable<String, POSRulesHashtableValues>();

		System.out.println("Creating positive rules...");
		checkReviews (getPosTrainRuleFilepath(), getPosTrainRuleFilenames(), 
				1, tagger, SentimentRulesMap);
		System.out.println("Creating negative rules...");
		checkReviews (getNegTrainRuleFilepath(), getNegTrainRuleFilenames(), 
				0, tagger, SentimentRulesMap);
		FileWriter writer = new FileWriter(new File(getResultsDirectory() 
				+ "/" + "POSRulesHashtable.txt"), true);
		storeHashtable(SentimentRulesMap, writer);
		writer.close();

	}

	public Hashtable <String, POSRulesHashtableValues> loadPOSRulesHashtable
	(String POSRulesHashtableFilepath) 
			throws ClassNotFoundException, IOException{

		Hashtable <String, POSRulesHashtableValues> hash = 
				new Hashtable <String, POSRulesHashtableValues>();
		BufferedReader br = new BufferedReader
				(new FileReader(POSRulesHashtableFilepath));
		String key;
		int pos, neg;
		for(String line; (line = br.readLine()) != null; ) {
			String[] pattern = line.split("\t");
			key = pattern[0];
			pos = Integer.parseInt(pattern[1]);
			neg = Integer.parseInt(pattern[2]);
			int thresholdNoOfAppearances = getThresholdNoOfAppearances();
			double thresholdPercentage = getThresholdPercentage();
			int total_appearances =  pos + neg;
			if (total_appearances >= thresholdNoOfAppearances)
				if (((pos*1.0/total_appearances)*100)>=thresholdPercentage 
				|| ((neg*1.0/total_appearances)*100)>=thresholdPercentage)
					hash.put(key, new POSRulesHashtableValues(pos, neg, true));
		}
		br.close();
		return hash;
	}

	public void storeHashtable(Hashtable<String, POSRulesHashtableValues> 
	SentimentRulesHashtable, FileWriter writer) 
			throws ClassNotFoundException, IOException{

		List<String> list = new ArrayList<String>();

		for (String key : SentimentRulesHashtable.keySet()){
			POSRulesHashtableValues values = SentimentRulesHashtable.get(key);
			int pos = values.getPosCounter();
			int neg = values.getNegCounter();
			int total_appearances =  pos + neg;
			int thresholdNoOfAppearances = getThresholdNoOfAppearances();
			double thresholdPercentage = getThresholdPercentage();

			if (total_appearances < thresholdNoOfAppearances)
				list.add(key);
			else{
				if (((pos*1.0/total_appearances)*100)>=thresholdPercentage){
					values.setActive(true);
					SentimentRulesHashtable.put(key, values);
					writer.write(key + "\t" + pos + "\t" + neg + "\n");
				}
				else if (((neg*1.0/total_appearances)*100)>=thresholdPercentage){
					writer.write(key + "\t" + pos + "\t" + neg + "\n");
					values.setActive(true);
					SentimentRulesHashtable.put(key, values);
				}
				else
					list.add(key);
			}
		}
		for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
			String key = iterator.next();
			SentimentRulesHashtable.remove(key);
		}
	}

	private void checkReviews (String reviewsFilepath, 
			ArrayList <String > reviewFilenames, int sentiment, 
			MaxentTagger tagger, Hashtable<String, 
			POSRulesHashtableValues> SentimentRulesMap) 
					throws IOException, ClassNotFoundException {

		String[] array = {".","..","...","?","??","???","!","!!","!!!","<br /><br />"};
		String filepath;
		if (sentiment == 1) filepath = getPosTrainRuleFilepath();
		else filepath = getNegTrainRuleFilepath();
		for (String s_file: reviewFilenames){
			Reader reader = new BufferedReader(new FileReader(filepath +"/" + s_file));
			DocumentPreprocessor dp = new DocumentPreprocessor(reader);
			dp.setSentenceFinalPuncWords(array);
			for (List<HasWord> sentence : dp) {
				String tagSentence = sentencePattern(sentence, tagger);
				addPattern(tagSentence, sentiment, SentimentRulesMap);
			}
			reader.close();
		}
	}

	private void addPattern(String tagSentence, int sentiment, 
			Hashtable<String, POSRulesHashtableValues> SentimentRulesMap) 
					throws IOException{

		if (!(tagSentence.equals("") ||tagSentence.equals(" "))){
			POSRulesHashtableValues values;
			if (SentimentRulesMap.containsKey(tagSentence))
				values = SentimentRulesMap.get(tagSentence);
			else
				values = new POSRulesHashtableValues();
			if(sentiment==1)
				values.setPosCounter(values.getPosCounter()+1);
			else
				values.setNegCounter(values.getNegCounter()+1);
			SentimentRulesMap.put(tagSentence,values);
		}
	}

	public String sentencePattern(List<HasWord> sentence, MaxentTagger tagger) 
			throws IOException{
		List<TaggedWord> tSentence = tagger.tagSentence(sentence);
		SentiWordNet sent = new SentiWordNet("SentiWordNet.txt");
		String strword, wordtag, wtag, modified_tSentence ="";
		int counter=1;
		for(TaggedWord word : tSentence){
			strword = word.word();
			if (strword.equals("<br />") || strword.equals("<br /><br />"))
				continue;
			if (!(strword.equals("n't") || strword.equals("not"))){
				wordtag = word.tag().toString();
				wtag = wordtag.substring(0,1).toLowerCase();
				if (wordtag.equals("RP")) continue;
				else if(wtag.equals("j")) {
					wtag = "a";
					wordtag = "ADJECTIVE";
				}
				else if (wtag.equals("n")) wordtag="NOUN";
				else if (wtag.equals("r")) wordtag="ADVERB";
				else if (wtag.equals("v")) wordtag="VERB";
				else continue;
				double sent_value = sent.extract(strword, wtag);
				String sent_str;
				if (sent_value > 0)	sent_str = "POS_";
				else if (sent_value < 0) sent_str = "NEG_";
				else sent_str = "NEU_";
				String tag = sent_str + wordtag;
				if (!(modified_tSentence.equals(""))) 
					modified_tSentence+=" " + tag + counter; 
				else 
					modified_tSentence+= tag + counter;
			}
			else{
				if (!modified_tSentence.equals("")) 
					modified_tSentence+=" NOT"+counter;
				else 
					modified_tSentence+="NOT"+counter;
			}
			counter++;
		}
		return modified_tSentence;
	}

	public int[] patternMatches(String filepath) throws IOException{
		String[] array = {".","..","...","?","??","???","!","!!","!!!","<br /><br />"};
		Reader reader = new BufferedReader(new FileReader(filepath));
		DocumentPreprocessor dp = new DocumentPreprocessor(reader);
		dp.setSentenceFinalPuncWords(array);

		int pos_counter=0;
		int neg_counter=0;
		String pattern;

		for (List<HasWord> sentence : dp) {
			pattern = sentencePattern(sentence, tagger);
			if (getPOSRulesHashtable().containsKey(pattern)){
				POSRulesHashtableValues values = getPOSRulesHashtable().get(pattern);
				if (values.isActive()){
					if (values.getPosCounter()>values.getNegCounter()) pos_counter++;
					else neg_counter++;
				}
			}
		}
		reader.close();
		int[] patterns = new int[2];
		patterns[0] = pos_counter;
		patterns[1] = neg_counter;

		return patterns;

	}

	public Object[] patternMatchesFromString(String text) throws IOException{
		String[] array = {".","..","...","?","??","???","!","!!","!!!","<br />","<br /><br />"};
		Reader reader = new StringReader(text);
		DocumentPreprocessor dp = new DocumentPreprocessor(reader);
		dp.setSentenceFinalPuncWords(array);

		int pos_counter=0;
		int neg_counter=0;
		String pattern;
		ArrayList<String> patterns_list = new ArrayList<String>();
		for (List<HasWord> sentence : dp) {
			pattern = sentencePattern(sentence, tagger);
			patterns_list.add(pattern);
			if (POSRulesHashtable.containsKey(pattern)){
				POSRulesHashtableValues values = POSRulesHashtable.get(pattern);
				if (values.isActive()){
					if (values.getPosCounter()>values.getNegCounter())
						pos_counter++;
					else 
						neg_counter++;
				}
			}
		}
		reader.close();
		Object[] patterns = new Object[3];
		patterns[0] = pos_counter;
		patterns[1] = neg_counter;
		patterns[2] = patterns_list;
		return patterns;

	}

	public int getMinPosRating() {
		return minPosRating;
	}

	public void setMinPosRating(int minPosRating) {
		this.minPosRating = minPosRating;
	}

	public int getMaxPosRating() {
		return maxPosRating;
	}

	public void setMaxPosRating(int maxPosRating) {
		this.maxPosRating = maxPosRating;
	}

	public int getMinNegRating() {
		return minNegRating;
	}

	public void setMinNegRating(int minNegRating) {
		this.minNegRating = minNegRating;
	}

	public int getMaxNegRating() {
		return maxNegRating;
	}

	public void setMaxNegRating(int maxNegRating) {
		this.maxNegRating = maxNegRating;
	}

	public double getPosRate() {
		return posRate;
	}

	public void setPosRate(double posRate) {
		this.posRate = posRate;
	}

	public ArrayList<String> getPosTrainRuleFilenames() {
		return posTrainRuleFilenames;
	}

	public void setPosTrainRuleFilenames(ArrayList<String> posTrainRuleFilenames) {
		this.posTrainRuleFilenames = posTrainRuleFilenames;
	}

	public ArrayList<String> getNegTrainRuleFilenames() {
		return negTrainRuleFilenames;
	}

	public void setNegTrainRuleFilenames(ArrayList<String> negTrainRuleFilenames) {
		this.negTrainRuleFilenames = negTrainRuleFilenames;
	}

	public boolean isShuffle() {
		return shuffle;
	}

	public void setShuffle(boolean shuffle) {
		this.shuffle = shuffle;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}

	public File getResultsDirectory() {
		return resultsDirectory;
	}

	public void setResultsDirectory(File resultsDirectory) {
		this.resultsDirectory = resultsDirectory;
	}

	public String getPosTrainRuleFilepath() {
		return posTrainRuleFilepath;
	}

	public void setPosTrainRuleFilepath(String posTrainRuleFilepath) {
		this.posTrainRuleFilepath = posTrainRuleFilepath;
	}

	public String getNegTrainRuleFilepath() {
		return negTrainRuleFilepath;
	}

	public void setNegTrainRuleFilepath(String negTrainRuleFilepath) {
		this.negTrainRuleFilepath = negTrainRuleFilepath;
	}

	public int getNoOfTrainRuleReviews() {
		return noOfTrainRuleReviews;
	}

	public void setNoOfTrainRuleReviews(int noOfTrainRuleReviews) {
		this.noOfTrainRuleReviews = noOfTrainRuleReviews;
	}

	public void writeFilenames(ArrayList<String> filenames, String outputFile) 
			throws IOException {
		FileWriter writer = new FileWriter(
				new File(getResultsDirectory() + "/" + outputFile), true);

		for (String s: filenames)
			writer.write(s +  "\n");

		writer.close();
	}

	public MaxentTagger getTagger() {
		return tagger;
	}

	public void setTagger(MaxentTagger tagger) {
		this.tagger = tagger;
	}

	public Hashtable <String, POSRulesHashtableValues> getPOSRulesHashtable() {
		return POSRulesHashtable;
	}

	public void setPOSRulesHashtable(Hashtable<String, POSRulesHashtableValues> POSRulesHashtable) {
		this.POSRulesHashtable = POSRulesHashtable;
	}

	public SentiWordNet getSentiWordNet() {
		return SentiWordNet;
	}

	public void setSentiWordNet(SentiWordNet sentiWordNet) {
		SentiWordNet = sentiWordNet;
	}

	public int getThresholdNoOfAppearances() {
		return thresholdNoOfAppearances;
	}

	public void setThresholdNoOfAppearances(int thresholdNoOfAppearances) {
		this.thresholdNoOfAppearances = thresholdNoOfAppearances;
	}

	public double getThresholdPercentage() {
		return thresholdPercentage;
	}

	public void setThresholdPercentage(double thresholdPercentage) {
		this.thresholdPercentage = thresholdPercentage;
	}

}
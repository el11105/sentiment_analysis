package sentimentanalysis;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import gr. demokritos .iit.jinsect.structs.GraphSimilarity;

import java .io. BufferedReader;
import java .io. BufferedWriter;
import java .io. FileReader;
import java .io. FileWriter;
import java .io. IOException;
import java.util.ArrayList;

public class AttributeRelationFile {

	private String relationName;
	private ArrayList < Attribute > attributes;
	private Instances instances;

	public AttributeRelationFile (String relationName) {
		this.relationName = relationName;
	}

	public void createFile (WordGraphsSimilarities values, POSRulesHashtable SentimentPOSRules, String posFilepath,
			String negFilepath, ArrayList <String > posReviewFilenames,
			ArrayList <String > negReviewFilenames) throws IOException, ClassNotFoundException {

		createAttributes();
		addHeader();
		addData (values, SentimentPOSRules, posFilepath, negFilepath, posReviewFilenames,
				negReviewFilenames);
	}
	
	public void createFile (String file) throws IOException {
		createRelativeAttributes();
		addHeader();
		addData (file);
	}

	private void createAttributes() {
		attributes = new ArrayList < Attribute >();
		attributes .add(new Attribute (" PositiveContainmentSimilarity "));
		attributes .add(new Attribute (" PositiveNormalizedValueSimilarity "));
		attributes .add(new Attribute (" PositiveValueSimilarity "));
		attributes .add(new Attribute (" NegativeContainmentSimilarity "));
		attributes .add(new Attribute (" NegativeNormalizedValueSimilarity "));
		attributes .add(new Attribute (" NegativeValueSimilarity "));
		attributes.add(new Attribute(" PositiveRulesMatched "));
		attributes.add(new Attribute(" NegativeRulesMatched "));

		ArrayList <String > sentimentValues = new ArrayList <String >();
		sentimentValues .add("0");
		sentimentValues .add("1");
		attributes .add(new Attribute (" Sentiment ", sentimentValues));
	}

	private void createRelativeAttributes() {
		attributes = new ArrayList < Attribute >();
		attributes .add(new Attribute (" RelativeContainmentSimilarity "));
		attributes .add(new Attribute (" RelativeNormalizedValueSimilarity "));
		attributes .add(new Attribute (" RelativeValueSimilarity "));
		ArrayList <String > sentimentValues = new ArrayList <String >();
		sentimentValues.add("0");
		sentimentValues.add("1");
		attributes .add(new Attribute (" Sentiment ", sentimentValues));
	}

	private void addHeader() {
		instances = new Instances (relationName, attributes, 0);
	}

	private void addData (WordGraphsSimilarities values, POSRulesHashtable SentimentPOSRules, String posFilepath,
			String negFilepath, ArrayList <String > posReviewFilenames,
			ArrayList <String > negReviewFilenames) throws IOException, ClassNotFoundException {
		
		addInstances (values.getPosModelGraph(), SentimentPOSRules, posFilepath,
				posReviewFilenames, values, 1);
		addInstances (values.getNegModelGraph(), SentimentPOSRules, negFilepath,
				negReviewFilenames, values, 0);
	}

	private void addData (String file) throws IOException {

		BufferedReader reader = new BufferedReader (new FileReader (file));
		for (int line = 0; line < 11; line ++)
			reader.readLine();

		String line = reader.readLine();
		while (line != null) {
			String [] values = line.split (",");
			double [] simValues = new double [values.length ];
			for (int index = 0; index < values.length - 1; index ++)
				simValues [index ] = Double.parseDouble (values [index ]);
			simValues [values.length - 1] =
					Integer.parseInt (values [values.length - 1]);

			addInstance (simValues);
			line = reader.readLine();
		}
		reader.close();
	}

	private void addInstances (ModelWordGraph graph, POSRulesHashtable POSRulesHashtable, String reviewsFilepath,
			ArrayList <String > reviewFilenames, WordGraphsSimilarities
			values, int sentiment) throws IOException, ClassNotFoundException {

		ReviewWordGraph reviewGraph = new ReviewWordGraph (graph.getWindow(),
				graph.getReviewsGraph(). isPreprocess());
		
		for (String s: reviewFilenames) {
			String full_path = reviewsFilepath.concat (s);
			reviewGraph.createGraph (full_path);
			values.graphsSimilaritiesWith (reviewGraph);
			
			int patterns[];
			patterns = POSRulesHashtable.patternMatches(full_path);
			int pos_counter = patterns[0];
			int neg_counter = patterns[1];

			addInstance (values.getPosGraphSimilarities(),
					values.getNegGraphSimilarities(), pos_counter, neg_counter, sentiment);
		}
	}

	private void addInstance (GraphSimilarity posGraphSim,
			GraphSimilarity negGraphSim, int pos_pattern, int neg_pattern, int sentiment) {

		double [] instance = new double [instances.numAttributes() ];

		instance [0] = posGraphSim.ContainmentSimilarity;
		instance [1] = posGraphSim.ValueSimilarity / posGraphSim.SizeSimilarity;
		instance [2] = posGraphSim.ValueSimilarity;

		instance [3] = negGraphSim.ContainmentSimilarity;
		instance [4] = negGraphSim.ValueSimilarity / negGraphSim.SizeSimilarity;
		instance [5] = negGraphSim.ValueSimilarity;
		
		instance [6] = pos_pattern;
		instance [7] = neg_pattern;
		instance [8] = sentiment;

		instances .add(new DenseInstance (1.0, instance));
	}

	private void addInstance (double [] simValues) {
		double [] instance = new double [instances.numAttributes() ];

		instance [0] = dsim (simValues [0], simValues [3]);
		instance [1] = dsim (simValues [1], simValues [4]);
		instance [2] = dsim (simValues [2], simValues [5]);

		instance [3] = simValues [6];

		instances .add(new DenseInstance (1.0, instance));
	}

	private int dsim (double posSim, double negSim) {
		int equal = 0;
		int positive = 1;
		int negative = 2;

		if (posSim < negSim) return negative;
		else if (posSim > negSim) return positive;
		else return equal;
	}


	public void storeToFile (String outputFile) throws IOException {
		BufferedWriter writer = new BufferedWriter (new
				FileWriter (outputFile));
		writer.write (instances.toString());
		writer.flush();
		writer.close();
	}

	public Instances getInstances() {
		return instances;
	}
}
/*
Copyright 2017 NTUA/ICCS

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files 
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, 
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package sentimentanalysis;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import gr.demokritos.iit.jinsect.structs.GraphSimilarity;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public class SentimentAnalysisImp {

	int window;
	boolean preprocess;
	WordGraphsSimilarities values;
	Instances instances;
	Classifier classifier;
	POSRulesHashtable SentimentPOSRulesHashtable;

	public SentimentAnalysisImp() {
		String relationName = "Sentiment_of_Similarities_of_WordGraphs";
		ArrayList<Attribute> attributes = new ArrayList<>(); 
		attributes.add(new Attribute(" PositiveContainmentSimilarity "));
		attributes.add(new Attribute(" PositiveNormalizedValueSimilarity "));
		attributes.add(new Attribute(" PositiveValueSimilarity "));
		attributes.add(new Attribute(" NegativeContainmentSimilarity "));
		attributes.add(new Attribute(" NegativeNormalizedValueSimilarity "));
		attributes.add(new Attribute(" NegativeValueSimilarity "));
		attributes.add(new Attribute(" PositiveRulesMatch "));
		attributes.add(new Attribute(" NegativeRulesMatch "));
		ArrayList<String> sentimentValues = new ArrayList<>();

		sentimentValues.add("0");
		sentimentValues.add("1");

		attributes.add(new Attribute("Sentiment", sentimentValues));
		Instances instances = new Instances(relationName, attributes, 0);
		instances.setClassIndex(instances.numAttributes() - 1);
		setInstances(instances);
	}

	@SuppressWarnings("unchecked")
	public int findSentiment(String text) throws Exception, IOException, ClassNotFoundException {
		ReviewWordGraph reviewGraph = new ReviewWordGraph(window, preprocess);
		reviewGraph.createGraphFromString(text);
		values.graphsSimilaritiesWith(reviewGraph);

		Object patterns[];
		patterns = SentimentPOSRulesHashtable.patternMatchesFromString(text);
		int pos_counter = (int) patterns[0];
		int neg_counter = (int) patterns[1];
		ArrayList<String> patterns_list = (ArrayList<String>) patterns[2];

		Instance inst = getInstance(values, pos_counter, neg_counter);
		int sentiment;
		try {
			sentiment = (int) classifier.classifyInstance(inst);

		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}

		for(String string : patterns_list) {
			POSRulesHashtableValues values;
			if (SentimentPOSRulesHashtable.getPOSRulesHashtable().containsKey(string))
				values = SentimentPOSRulesHashtable.getPOSRulesHashtable().get(string);
			else
				values = new POSRulesHashtableValues();
			if(sentiment==1)
				values.setPosCounter(values.getPosCounter()+1);
			else
				values.setNegCounter(values.getNegCounter()+1);
			SentimentPOSRulesHashtable.getPOSRulesHashtable().put(string,values);

		}

		return sentiment;
	}

	public POSRulesHashtable getSentimentPOSRulesHashtable() {
		return SentimentPOSRulesHashtable;
	}

	public void setSentimentPOSRulesHashtable(POSRulesHashtable SentimentPOSRulesHashtable) {
		this.SentimentPOSRulesHashtable = SentimentPOSRulesHashtable;
	}

	public int getWindow() {
		return window;
	}

	public void setWindow(int window) {
		this.window = window;
	}

	public boolean isPreprocess() {
		return preprocess;
	}

	public void setPreprocess(boolean preprocess) {
		this.preprocess = preprocess;
	}

	public WordGraphsSimilarities getValues() {
		return values;
	}

	public void setValues(WordGraphsSimilarities values) {
		this.values = values;
	}

	public Classifier getClassifier() {
		return classifier;
	}

	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}

	public void add(Instance instance){
		instances.add(instance);
	}

	public Instance getInstance(WordGraphsSimilarities values, int pos, int neg){

		GraphSimilarity posGraphSim = values.getPosGraphSimilarities();
		GraphSimilarity negGraphSim = values.getNegGraphSimilarities();

		Instance instance = new DenseInstance(instances.numAttributes());
		instance.setDataset(instances);

		instance.setValue(0, posGraphSim.ContainmentSimilarity);
		instance.setValue(1, posGraphSim.ValueSimilarity / posGraphSim.SizeSimilarity);
		instance.setValue(2, posGraphSim.ValueSimilarity);

		instance.setValue(3, negGraphSim.ContainmentSimilarity);
		instance.setValue(4, negGraphSim.ValueSimilarity / negGraphSim.SizeSimilarity);
		instance.setValue(5, negGraphSim.ValueSimilarity);
		instance.setValue(6, pos);
		instance.setValue(7, neg);
		return instance;
	}

	public void storeToFile(String outputFile) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
		writer.write(instances.toString());
		writer.flush();
		writer.close();
	}

	public Instances getInstances() {
		return instances;
	}

	public void setInstances(Instances instances) {
		this.instances = instances;
	}

}

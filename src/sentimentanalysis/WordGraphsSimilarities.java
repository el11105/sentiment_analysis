/**
 * 
 * @author pkiourti
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.IOException;
import java.io.InputStream;

import gr.demokritos.iit.jinsect.documentModel.comparators.NGramCachedGraphComparator;
import gr.demokritos.iit.jinsect.structs.GraphSimilarity;


public class WordGraphsSimilarities {
	
	private GraphSimilarity posGraphSimilarities;
	private ModelWordGraph posModelGraph;

	private GraphSimilarity negGraphSimilarities;
	private ModelWordGraph negModelGraph;
	
	public WordGraphsSimilarities(String posGraphFile, String negGraphFile) 
			throws ClassNotFoundException, IOException { 
		posModelGraph = new ModelWordGraph();
		posModelGraph.loadModelGraph(posGraphFile);
		negModelGraph = new ModelWordGraph();
		negModelGraph.loadModelGraph(negGraphFile);
	}
	
	public WordGraphsSimilarities(InputStream posGraphFileInputStream, InputStream negGraphFileInputStream) 
			throws ClassNotFoundException, IOException { 
		posModelGraph = new ModelWordGraph();
		posModelGraph.loadModelGraph(posGraphFileInputStream);
		negModelGraph = new ModelWordGraph();
		negModelGraph.loadModelGraph(negGraphFileInputStream);
	}
	
	public WordGraphsSimilarities(ModelWordGraph posModelGraph, 
			ModelWordGraph negModelGraph) { 
		this.posModelGraph = posModelGraph;
		this.negModelGraph = negModelGraph;
	}

	public void graphsSimilaritiesWith(ReviewWordGraph reviewGraph) {
		
		NGramCachedGraphComparator reviewGraphComparator = 
				new NGramCachedGraphComparator();
		
		posGraphSimilarities = reviewGraphComparator.getSimilarityBetween(
				reviewGraph.getGraph(), 
				posModelGraph.getReviewsGraph().getGraph());
		
		negGraphSimilarities = reviewGraphComparator.getSimilarityBetween(
				reviewGraph.getGraph(), 
				negModelGraph.getReviewsGraph().getGraph());
	}

	public void removeCommonSubGraphFromSentimentGraphs(boolean remove) {
		if (remove == false)
			return;
		ReviewWordGraph[] newModels = 
				posModelGraph.removeCommonSubgraph(
						negModelGraph.getReviewsGraph());
		posModelGraph.setReviewsGraph(newModels[0]);
		negModelGraph.setReviewsGraph(newModels[1]);
	}
	
	public GraphSimilarity getPosGraphSimilarities() {
		return posGraphSimilarities;
	}
	
	public GraphSimilarity getNegGraphSimilarities() {
		return negGraphSimilarities;
	}

	public ModelWordGraph getPosModelGraph() {
		return posModelGraph;
	}
	
	public ModelWordGraph getNegModelGraph() {
		return negModelGraph;
	}
}
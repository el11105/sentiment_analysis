/**
 * 
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import gr.demokritos.iit.jinsect.utils;


public class ReviewWordGraph implements Serializable {

	private static final long serialVersionUID = 1L;
	private DocumentWordGraph reviewGraph;
	private boolean preprocess;
	
	public ReviewWordGraph(int window, boolean preprocess) {
		reviewGraph = new DocumentWordGraph(window);
		this.preprocess = preprocess;
	}
	
	public void createGraph(String reviewFile) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(reviewFile));
		
		if (preprocess) {
			String imdbReview = removeSpecialCharacters(reader.readLine());
			reviewGraph.setDataString(imdbReview);
		} else
			reviewGraph.setDataString(reader.readLine());
		reader.close();
	}

	public void createGraphFromString(String reviewSentence) throws IOException {
		if (preprocess) {
			String imdbReview = removeSpecialCharacters(reviewSentence);
			reviewGraph.setDataString(imdbReview);
		} else
			reviewGraph.setDataString(reviewSentence);
	}
		
	private String removeSpecialCharacters(String review) {
		
		String pattern = "\\w|\\s";
		char[] reviewCharacters = review.toCharArray();
		char[] filteredReview = new char[review.length()];
		int size = 0;
		for (int index = 0; index < review.length(); index++)
			if (String.valueOf(reviewCharacters[index]).matches(pattern))
				filteredReview[size++] = reviewCharacters[index];
		
		char[] returnReview = new char[size];
		for (int i = 0; i < size; i++)
			returnReview[i] = filteredReview[i];
		
		String imdbReview = String.valueOf(returnReview);
		return imdbReview.toLowerCase();
	}
	
	public void merge(ReviewWordGraph g, double weight) {
		reviewGraph.merge(g.getGraph(), weight);
	}

	public DocumentWordGraph[] removeCommonSubgraph(DocumentWordGraph g2) {

		DocumentWordGraph newGraphs[] = new DocumentWordGraph[2];
    	newGraphs[0] = (DocumentWordGraph) reviewGraph.clone();
    	newGraphs[1] = (DocumentWordGraph) g2.clone();
    	
		DocumentWordGraph commonSubgraph = newGraphs[0].intersectGraph(
				newGraphs[1]);
    	
    	newGraphs[0] = newGraphs[0].allNotIn(commonSubgraph);
    	newGraphs[1] = newGraphs[1].allNotIn(commonSubgraph);
    	commonSubgraph = newGraphs[0].intersectGraph(newGraphs[1]);
    	
    	return newGraphs;
	}

	public void printToFile(String filepath) 
			throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter writer = new PrintWriter(filepath, "UTF-8");
		writer.println(utils.graphToDot(reviewGraph.getGraphLevel(0), true));
		writer.close();
	}
	
	public void printToSystemOutput() {
		System.out.println(utils.graphToDot(reviewGraph.getGraphLevel(0), 
				true));
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(reviewGraph);
	}
	
	private void readObject(ObjectInputStream in) 
			throws IOException, ClassNotFoundException {
		reviewGraph = (DocumentWordGraph) in.readObject();
	}

	public DocumentWordGraph getGraph() {
		return reviewGraph;
	}

	public int getNoOfNodes() {
		return reviewGraph.getGraphLevel(0).getVerticesCount();
	}
	
	public int getNoOfEdges() {
		return reviewGraph.getGraphLevel(0).getEdgesCount();
	}
	
	public void setGraph(DocumentWordGraph newGraph) {
		reviewGraph = newGraph;
	}
	
	public boolean isPreprocess() {
		return preprocess;
	}

	public void setPreprocess(boolean preprocess) {
		this.preprocess = preprocess;
	}
}
/**
 * 
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Properties;

public class ModelGraphs {

	private int noOfWordGraphsReviews;
	
	private int nSize;
	private int window;
	
	private boolean remove;
	private boolean shuffle;
	private boolean preprocess;
	
	private long seed;
	private int minPosRating;
	private int maxPosRating;
	private int minNegRating;
	private int maxNegRating;
	
	private String posGraphFilepath;
	private String negGraphFilepath;
	
	private int graphRangeId;
	private File resultsDirectory;
	private File infoFile;
	
	private String posWordGraphBinaryFile;
	private String negWordGraphBinaryFile;
	
	private ArrayList<String> posGraphFilenames;
	private ArrayList<String> negGraphFilenames;
	
	public static void main(String[] args) throws IOException {
		if (args.length != 3)
			return;
		
		InputStream reader = new FileInputStream(args[0]);
		Properties properties = new Properties();
		
		if (args[0].contains(".xml"))
			properties.loadFromXML(reader);
		else
			properties.load(reader);
		
		boolean stage1 = Boolean.parseBoolean(args[1]);
		boolean allStages = Boolean.parseBoolean(args[2]);
		
		System.out.println("Running First Stage of Sentiment "
				+ "Classification: Creating Graphs\n");

		long start = System.currentTimeMillis();

		ModelGraphs object = new ModelGraphs();
		
		object.setParameters(properties, true);
		object.createResultsDirectory();
		object.setFiles(allStages);
		
		object.findGraphFilenames(object.getNoOfWordGraphsReviews(), stage1);
		
		long wordGraphs = System.currentTimeMillis();

		object.createWordGraphs(allStages, stage1);

		long end = System.currentTimeMillis();
		
		NumberFormat formatter = new DecimalFormat("#0.00000");
		
		FileWriter output = new FileWriter(object.getInfoFile(), true);
		
		output.write("Time for word graphs: "); 
		output.write(formatter.format((end - wordGraphs) / 1000d) + " seconds\n");
		output.write("\nExecution time is ");
		output.write(formatter.format((end - start) / 1000d) + " seconds\n");
		output.close();
		
		System.out.println("\nwindowSize is" + object.getWindow() + "\n\n");
		System.out.println("\nExecution time is ");
		System.out.println(formatter.format((end - start) / 1000d) + " seconds");
		
		return;	
	}

	public void setParameters(Properties properties, 
			boolean onlyThisStage) {
		
		System.out.println("Setting parameters...");
		setNoOfWordGraphsReviews(Integer.parseInt(
				properties.getProperty("noOfGraphReviews")));
		
		setWindow(Integer.parseInt(properties.getProperty("windowSize")));
		
		setMinPosRating(Integer.parseInt(
				properties.getProperty("minPositiveRating")));
		setMaxPosRating(Integer.parseInt(
				properties.getProperty("maxPositiveRating")));
		setMinNegRating(Integer.parseInt(
				properties.getProperty("minNegativeRating")));
		setMaxNegRating(Integer.parseInt(
				properties.getProperty("maxNegativeRating")));
		
		if (properties.getProperty("seed") != null)
			setSeed(Long.parseLong(properties.getProperty("seed")));
		else
			setSeed(-1);
		setShuffle(Boolean.parseBoolean(properties.getProperty("shuffle")));
		setRemove(Boolean.parseBoolean(properties.getProperty("remove")));
		setPreprocess(Boolean.parseBoolean(
				properties.getProperty("preprocess")));
		
		setPosGraphFilepath(properties.getProperty("positiveGraphFilepath"));
		setNegGraphFilepath(properties.getProperty("negativeGraphFilepath"));
	}
	
	public void createResultsDirectory() {
		System.out.println("Creating directory for results...");
		String preprocessing = "NP", removing = "NR", results;
		if (isRemove())
			removing = "R";
		if (isPreprocess())
			preprocessing = "P";
		
		results = getWindow() + "_" + removing + "_" + preprocessing + "_" + "_Graphs";
		
		setResultsDirectory(new File(results));
		getResultsDirectory().mkdir();
	}
	
	public void setFiles(boolean allStages) {
		System.out.println("Setting the output filenames...");
		
		graphRangeId = 0;
		String filePrefix = getResultsDirectory() + "/";
		String fileSuffix = "_" + getNoOfWordGraphsReviews() + "_posWordGraph";

		if (allStages) {
			setPosWordGraphBinaryFile(filePrefix + "BinaryPositiveWordGraph" 
					+ getNoOfWordGraphsReviews());
			setNegWordGraphBinaryFile(filePrefix + "BinaryNegativeWordGraph" 
					+ getNoOfWordGraphsReviews());
			setInfoFile(new File(filePrefix + "Info.txt"));
		} else {
			while (new File(filePrefix + graphRangeId + fileSuffix).isFile())
				graphRangeId++;
			
			setPosWordGraphBinaryFile(filePrefix + graphRangeId + fileSuffix);
			
			fileSuffix = "_" + getNoOfWordGraphsReviews() + "_negWordGraph";
			setNegWordGraphBinaryFile(filePrefix + graphRangeId + fileSuffix);
			
			
			fileSuffix = "_" + getNoOfWordGraphsReviews() + "_Info.txt";
			setInfoFile(new File(filePrefix + graphRangeId + fileSuffix));
		}
	}

	public void findGraphFilenames(int noOfReviews, boolean stage) 
			throws IOException {
		
		System.out.println("Selecting reviews for graphs...");
		
		FilenamePattern pattern;
		pattern = new FilenamePattern(getMinPosRating(), getMaxPosRating());
		setPosGraphFilenames(pattern.findFilenames(getPosGraphFilepath(), 
				noOfReviews, isShuffle(), getSeed()));
			
		pattern = new FilenamePattern(getMinNegRating(), getMaxNegRating());
		setNegGraphFilenames(pattern.findFilenames(getNegGraphFilepath(), 
				noOfReviews, isShuffle(), getSeed()));
		
		if (stage) {
			writeFilenames(getPosGraphFilenames(), "FilenamesForPosGraph_" +
					graphRangeId + "_" + getNoOfWordGraphsReviews() + ".txt");
			writeFilenames(getNegGraphFilenames(), "FilenamesForNegGraph_" + 
					graphRangeId + "_" + getNoOfWordGraphsReviews() + ".txt");
		} else {
			writeFilenames(getPosGraphFilenames(), "FilenamesForPosGraph.txt");
			writeFilenames(getNegGraphFilenames(), "FilenamesForNegGraph.txt");
		}
	}

	public void writeFilenames(ArrayList<String> filenames, String outputFile) 
			throws IOException {
		
		FileWriter writer = new FileWriter(
				new File(getResultsDirectory() + "/" + outputFile), true);
		
		for (String s: filenames)
			writer.write(s +  "\n");
		
		writer.close();
	}

	public void createWordGraphs(boolean allStages, boolean stage1) 
			throws FileNotFoundException, IOException {
		
		System.out.println("Creating positive and negative model "
				+ "word graphs...");
		
		ModelWordGraph posGraph = createWordGraph(getPosGraphFilepath(), 
				getPosGraphFilenames());
		ModelWordGraph negGraph = createWordGraph(getNegGraphFilepath(), 
				getNegGraphFilenames());
		
		if (isRemove()) {
			WordGraphsSimilarities values = 
					new WordGraphsSimilarities(posGraph, negGraph);
			values.removeCommonSubGraphFromSentimentGraphs(isRemove());
			posGraph = values.getPosModelGraph();
			negGraph = values.getNegModelGraph();
		}
		
		storeWordGraphs(posGraph, negGraph);
		
		if (allStages) {
			String filePrefix = getResultsDirectory() + "/";
			String infoPos = "PositiveWordGraph";
			String infoNeg = "NegativeWordGraph";
			String suffix = getNoOfWordGraphsReviews() + ".txt";
			
			posGraph.printToFile(filePrefix + infoPos + suffix);
			negGraph.printToFile(filePrefix + infoNeg + suffix);
		} else if (stage1) {
			String filePrefix = getResultsDirectory() + "/";
			String infoPos = "PositiveWordGraph_" + graphRangeId + "_"; 
			String infoNeg = "NegativeWordGraph_" + graphRangeId + "_";
			String suffix = getNoOfWordGraphsReviews() + ".txt";
			
			posGraph.printToFile(filePrefix + infoPos + suffix);
			negGraph.printToFile(filePrefix + infoNeg + suffix);
		}
	}
	
	public ModelWordGraph createWordGraph(String reviewsFilepath, 
			ArrayList<String> reviewFilenames) 
					throws FileNotFoundException, IOException {
		
		ModelWordGraph graph;
		
		graph = new ModelWordGraph(getNoOfWordGraphsReviews(), getWindow());
		graph.createModelGraph(reviewsFilepath, reviewFilenames, 
				isPreprocess());
		
		return graph;
	}
	
	public void storeWordGraphs(ModelWordGraph posGraph, 
			ModelWordGraph negGraph) 
					throws IOException {
		
		FileWriter output =	new FileWriter(getInfoFile(), true);
		
		output.write("Nodes of pos wordGraph: " + posGraph.getNoOfNodes());
		output.write("\nEdges of pos wordGraph: " + posGraph.getNoOfEdges());
		output.write("\nNodes of neg wordGraph: " + negGraph.getNoOfNodes());
		output.write("\nEdges of neg wordGraph: " + negGraph.getNoOfEdges());
		output.write("\n");
		output.close();
		
		posGraph.storeModelGraph(getPosWordGraphBinaryFile());
		negGraph.storeModelGraph(getNegWordGraphBinaryFile());
	}
	
	public int getNoOfWordGraphsReviews() {
		return noOfWordGraphsReviews;
	}

	public void setNoOfWordGraphsReviews(int noOfWordGraphsReviews) {
		this.noOfWordGraphsReviews = noOfWordGraphsReviews;
	}
	
	public int getnSize() {
		return nSize;
	}

	public void setnSize(int nSize) {
		this.nSize = nSize;
	}

	public int getWindow() {
		return window;
	}

	public void setWindow(int window) {
		this.window = window;
	}
	
	public boolean isRemove() {
		return remove;
	}

	public void setRemove(boolean remove) {
		this.remove = remove;
	}

	public boolean isShuffle() {
		return shuffle;
	}

	public void setShuffle(boolean shuffle) {
		this.shuffle = shuffle;
	}

	public boolean isPreprocess() {
		return preprocess;
	}

	public void setPreprocess(boolean preprocess) {
		this.preprocess = preprocess;
	}
	
	public int getMinPosRating() {
		return minPosRating;
	}

	public void setMinPosRating(int minPosRating) {
		this.minPosRating = minPosRating;
	}

	public int getMaxPosRating() {
		return maxPosRating;
	}

	public void setMaxPosRating(int maxPosRating) {
		this.maxPosRating = maxPosRating;
	}

	public int getMinNegRating() {
		return minNegRating;
	}

	public void setMinNegRating(int minNegRating) {
		this.minNegRating = minNegRating;
	}

	public int getMaxNegRating() {
		return maxNegRating;
	}

	public void setMaxNegRating(int maxNegRating) {
		this.maxNegRating = maxNegRating;
	}
	
	public String getPosGraphFilepath() {
		return posGraphFilepath;
	}

	public void setPosGraphFilepath(String posGraphFilepath) {
		this.posGraphFilepath = posGraphFilepath;
	}

	public String getNegGraphFilepath() {
		return negGraphFilepath;
	}

	public void setNegGraphFilepath(String negGraphFilepath) {
		this.negGraphFilepath = negGraphFilepath;
	}

	public File getResultsDirectory() {
		return resultsDirectory;
	}

	public void setResultsDirectory(File resultsDirectory) {
		this.resultsDirectory = resultsDirectory;
	}
	

	public String getPosWordGraphBinaryFile() {
		return posWordGraphBinaryFile;
	}

	public void setPosWordGraphBinaryFile(String posWordGraphBinaryFile) {
		this.posWordGraphBinaryFile = posWordGraphBinaryFile;
	}

	public String getNegWordGraphBinaryFile() {
		return negWordGraphBinaryFile;
	}

	public void setNegWordGraphBinaryFile(String negWordGraphBinaryFile) {
		this.negWordGraphBinaryFile = negWordGraphBinaryFile;
	}
	
	public ArrayList<String> getPosGraphFilenames() {
		return posGraphFilenames;
	}

	public void setPosGraphFilenames(ArrayList<String> posGraphFilenames) {
		this.posGraphFilenames = posGraphFilenames;
	}

	public ArrayList<String> getNegGraphFilenames() {
		return negGraphFilenames;
	}

	public void setNegGraphFilenames(ArrayList<String> negGraphFilenames) {
		this.negGraphFilenames = negGraphFilenames;
	}
	
	public int getGraphRangeId() {
		return graphRangeId;
	}
	
	public void setGraphRangeId(int id) {
		this.graphRangeId = id;
	}

	public File getInfoFile() {
		return infoFile;
	}

	public void setInfoFile(File infoFile) {
		this.infoFile = infoFile;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}
}

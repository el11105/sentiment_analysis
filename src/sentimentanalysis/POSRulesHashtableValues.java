package sentimentanalysis;

public class POSRulesHashtableValues {


	private int posCounter;
	private int negCounter;
	private boolean isActive;

	POSRulesHashtableValues(){
		this.posCounter=0;
		this.negCounter=0;
		this.isActive=false;
	}

	POSRulesHashtableValues(int pos, int neg, boolean active){
		this.posCounter=pos;
		this.negCounter=neg;
		this.isActive=active;
	}
	public int getPosCounter() {
		return posCounter;
	}
	public void setPosCounter(int posCounter) {
		this.posCounter = posCounter;
	}
	public int getNegCounter() {
		return negCounter;
	}
	public void setNegCounter(int negCounter) {
		this.negCounter = negCounter;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}



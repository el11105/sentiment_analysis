/**
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class Run {

	private File resultsDirectory;
	
	public static boolean allStages = false;
	public static boolean stage1 = false;
	public static boolean stage1a = false;
	public static boolean stage2 = false;
	public static boolean stage3 = false;
	
	public static void main(String[] args) throws Exception {
		
		if (args.length != 1)
			return;

		Run run =  new Run();
		InputStream reader = new FileInputStream(args[0]);
		Properties properties = new Properties();
		if (args[0].contains(".xml"))
			properties.loadFromXML(reader);
		else
			properties.load(reader);
		
		String noTrainReviews = properties.getProperty("noOfTrainReviews");
		String classifier = properties.getProperty("classifierName");

		if (noTrainReviews != null && classifier != null)
			run.allStages(properties);
		
		else if (noTrainReviews == null && classifier == null)
			run.stage1(args);
		
		else if (classifier == null)
			run.stage2(args);
			
		else if (noTrainReviews == null)
			run.stage3(args);
		
	}

	public void createResultsDirectory(ModelGraphs graphs, DataFiles files, 
			Classifiers classes) {
		System.out.println("Creating directory for results...");
		
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);

		String removing = "NR", preprocessing = "NP", shuffling = "NS", 
				results;
		if (graphs.isRemove()) removing = "R";
		if (graphs.isPreprocess()) preprocessing = "P";
		if (graphs.isShuffle()) shuffling = "S";
		
		results = graphs.getNoOfWordGraphsReviews() + "_" + graphs.getWindow();
		results +=  "_" + removing + "_" + preprocessing + "_" + shuffling;
		results += "_" + shuffling + "_" + files.getNoTrainReviews() + "_";
		results += files.getNoTestReviews() + "_"+ classes.getClassifierName();
		results+= "_"+ classes.getThresholdPercentage() +"PER";
		results += "_" + day + "" + month + "" + year;
		results += "_" + hours + "." + minutes + "." + seconds;
		
		setResultsDirectory(new File(results));
		getResultsDirectory().mkdir();
	}
	
	public void stage1(String[] args) throws IOException, ClassNotFoundException {
		
		stage1 = true;
		String[] arg = new String[3];
		arg[0] = args[0];
		arg[1] = String.valueOf(stage1);
		arg[2] = String.valueOf(allStages);
		ModelGraphs.main(arg);
	}
	
	
	 public void stage1a(String[] args) throws IOException, ClassNotFoundException {
	 
		stage1a = true;
		String[] arg = new String[3];
		arg[0] = args[0];
		arg[1] = String.valueOf(stage1a);
		arg[2] = String.valueOf(allStages);
		
		POSRulesHashtable.main(arg);
	}
	
	
	public void stage2(String[] args) throws ClassNotFoundException, IOException {
		stage2 = true;
		String[] arg = new String[3];
		arg[0] = args[0];
		arg[1] = String.valueOf(stage2);
		arg[2] = String.valueOf(allStages);
		
		DataFiles.main(arg);
	}
	
	public void stage3(String[] args) throws Exception {
		stage3 = true;
		String[] arg = new String[3];
		arg[0] = args[0];
		arg[1] = String.valueOf(stage2);
		arg[2] = String.valueOf(allStages);
		
		Classifiers.main(arg);
	}
	
	public void allStages(Properties properties) throws Exception {
		allStages = true;

		System.out.println("Running all Stages of Sentiment "
				+ "Classification\n");

		long start = System.currentTimeMillis();
		
		ModelGraphs graphs = new ModelGraphs();
		DataFiles files = new DataFiles();
		Classifiers classifiers = new Classifiers();	
		
		setParameters(properties, graphs, files, classifiers);
		setFiles(graphs, files, classifiers);
		
		graphsStage(graphs, classifiers.getResultsFile());
		filesStage(graphs, files, classifiers.getResultsFile());		
		classifiersStage(files, classifiers);
		
		long end = System.currentTimeMillis();
		
		NumberFormat formatter = new DecimalFormat("#0.00000");
		FileWriter output = new FileWriter(new File(
				classifiers.getResultsFile()), true);
		
		output.write("\nExecution time is " + 
				formatter.format((end - start) / 1000d) + " seconds");
		output.close();
		System.out.print("\nExecution time is " + 
				formatter.format((end - start) / 1000d) + " seconds");
	}
	
	private void setParameters(Properties properties, ModelGraphs graphs, 
			DataFiles files, Classifiers classifiers) 
					throws ClassNotFoundException, IOException {
		
		graphs.setParameters(properties, false);
		files.setParameters(properties, false);
		classifiers.setParameters(properties, false);
		
		createResultsDirectory(graphs, files, classifiers);
		
		graphs.setResultsDirectory(getResultsDirectory());
		files.setResultsDirectory(getResultsDirectory());
		classifiers.setResultsDirectory(getResultsDirectory());
	}

	private void setFiles(ModelGraphs graphs, DataFiles files, 
			Classifiers classifiers) {
		graphs.setFiles(allStages);
		files.setFiles(allStages);
		classifiers.setFiles(allStages);
	}
	
	private void graphsStage(ModelGraphs graphs, String file) 
			throws IOException {

		graphs.findGraphFilenames(graphs.getNoOfWordGraphsReviews(), false);
		
		long wordGraphs = System.currentTimeMillis();
		graphs.createWordGraphs(allStages, stage1);
		long end = System.currentTimeMillis();
		
		NumberFormat formatter = new DecimalFormat("#0.00000");
		FileWriter output = new FileWriter(new File(file), true);
		output.write("Time for word graphs: "); 
		output.write(formatter.format((end - wordGraphs) / 1000d) + " seconds\n");
		output.close();
	}

	
	private void filesStage(ModelGraphs graphs, DataFiles files, String file) 
			throws IOException, ClassNotFoundException {

		files.setPosWordGraphBinaryFile(graphs.getPosWordGraphBinaryFile());
		files.setNegWordGraphBinaryFile(graphs.getNegWordGraphBinaryFile());
		files.setPosGraphFilenames(graphs.getPosGraphFilenames());
		files.setNegGraphFilenames(graphs.getNegGraphFilenames());

		files.findTrainFilenames(false);
		//files.findTestFilenames(false);
		long wordGraphs = System.currentTimeMillis();
		files.wordGraphsFilesARFF();
		long end = System.currentTimeMillis();
		NumberFormat formatter = new DecimalFormat("#0.00000");
		FileWriter output = new FileWriter(new File(file), true);
		output.write("Time for files of word graphs: "); 
		output.write(formatter.format((end - wordGraphs) / 1000d) + " seconds\n");
		output.close();
	}

	private void classifiersStage(DataFiles files, Classifiers classifiers) 
			throws Exception {

		classifiers.setWordGraphsTrainFile(files.getWordGraphsTrainFile());
		classifiers.setPosWordGraphBinaryFile(files.getPosWordGraphBinaryFile());
		classifiers.setNegWordGraphBinaryFile(files.getNegWordGraphBinaryFile());
		classifiers.findTestFilenames(false);
		System.out.println("Creating classifier ...");
		long wordGraphs = System.currentTimeMillis();
		classifiers.createClassifierForWordGraphs();

		System.out.println("Evaluating classifier...");
		long wordGraphs2 = System.currentTimeMillis();
		classifiers.evaluateClassifierForWordGraphs();
		long end = System.currentTimeMillis();
		NumberFormat formatter = new DecimalFormat("#0.00000");
		
		FileWriter output = new FileWriter(new File(classifiers.getResultsFile()), true);
		
		output.write("Time for classifier of word graphs: "); 
		output.write(formatter.format((wordGraphs2 - wordGraphs) / 1000d) + " seconds\n");
		output.write("Time for evaluating classifier of word graphs: "); 
		output.write(formatter.format((end - wordGraphs2) / 1000d) + " seconds\n");
		output.close();
	}

	public File getResultsDirectory() {
		return resultsDirectory;
	}

	public void setResultsDirectory(File resultsDirectory) {
		this.resultsDirectory = resultsDirectory;
	}
}

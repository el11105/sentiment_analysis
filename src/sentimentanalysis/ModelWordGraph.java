/**
 * 
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ModelWordGraph {

	private ReviewWordGraph reviewsGraph;
	private int noReviews;
	private int window;
	private ArrayList<String> reviewFilenames;

	public ModelWordGraph() { }
	
	public ModelWordGraph(int noReviews, int window) {
		this.noReviews = noReviews;
		this.window = window;
		reviewsGraph = null;
		reviewFilenames = null;
	}

    public void createModelGraph(String reviewFilepath, 
    		ArrayList<String> reviewFilenames, boolean preprocess) 
    		throws IOException {
    	
		ReviewWordGraph reviewGraph = new ReviewWordGraph(window, preprocess);
		reviewsGraph = new ReviewWordGraph(window, preprocess);
		this.reviewFilenames = new ArrayList<String>();
		
		int mergedReviews = 0;
		for (String s: reviewFilenames) {
			reviewGraph.createGraph(reviewFilepath.concat(s));
			reviewsGraph.merge(reviewGraph, 1 / (1 + mergedReviews));
			this.reviewFilenames.add(s);
			if (++mergedReviews >= noReviews)
				break;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadModelGraph(String inputGraphFile) 
			throws ClassNotFoundException, IOException {
		
		FileInputStream input = new FileInputStream(inputGraphFile);
		ObjectInputStream inputGraphStream = new ObjectInputStream(input);
		
		noReviews = inputGraphStream.readInt();
		window = inputGraphStream.readInt();
		reviewsGraph = ((ReviewWordGraph) inputGraphStream.readObject());
		reviewFilenames = (ArrayList<String>) inputGraphStream.readObject();
		inputGraphStream.close();
	}
	
	@SuppressWarnings("unchecked")
	public void loadModelGraph(InputStream input) 
			throws ClassNotFoundException, IOException {
		
		ObjectInputStream inputGraphStream = new ObjectInputStream(input);
		
		noReviews = inputGraphStream.readInt();
		window = inputGraphStream.readInt();
		reviewsGraph = ((ReviewWordGraph) inputGraphStream.readObject());
		reviewFilenames = (ArrayList<String>) inputGraphStream.readObject();
		inputGraphStream.close();
	}
	
	public void storeModelGraph(String outputGraphFile) throws IOException {
		FileOutputStream output = new FileOutputStream(outputGraphFile);
		ObjectOutputStream outputGraphStream = new ObjectOutputStream(output);
		
		outputGraphStream.writeInt(noReviews);
		outputGraphStream.writeInt(window);
		outputGraphStream.writeObject(reviewsGraph);
		outputGraphStream.writeObject(reviewFilenames);
		outputGraphStream.close();
	}
	
	public ReviewWordGraph[] removeCommonSubgraph(ReviewWordGraph graph2) {
		DocumentWordGraph[] wordGraphs = 
				reviewsGraph.removeCommonSubgraph(graph2.getGraph());
		reviewsGraph.setGraph(wordGraphs[0]);
		graph2.setGraph(wordGraphs[1]);
		
		ReviewWordGraph[] newGraphs = new ReviewWordGraph[2];
		newGraphs[0] = reviewsGraph;
		newGraphs[1] = graph2;
		return newGraphs;
	}
	
	public void printToFile(String filepath) 
			throws FileNotFoundException, UnsupportedEncodingException {
		reviewsGraph.printToFile(filepath);
	}
	
	public void printToSystemOutput() {
		reviewsGraph.printToSystemOutput();
	}
	
	public ReviewWordGraph getReviewsGraph() {
		return reviewsGraph;
	}
	
	public int getNoOfNodes() {
		return reviewsGraph.getNoOfNodes();
	}
	
	public int getNoOfEdges() {
		return reviewsGraph.getNoOfEdges();
	}
	
	public int getWindow() {
		return window;
	}

	public int getNoReviews() {
		return noReviews;
	}
	
	public void setReviewsGraph(ReviewWordGraph newReviewsGraph) {
		reviewsGraph = newReviewsGraph;
	}
	
	public ArrayList<String> getReviewFilenames() {
		return reviewFilenames;
	}
}

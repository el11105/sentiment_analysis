/**
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class Classifiers {

	private String wordGraphsTrainFile;

	private String posTestFilepath;
	private String negTestFilepath;
	private String taggerFilepath;
	private String POSRulesHashtableFilepath;
	private String SentiWordNetFilepath;

	private String posWordGraphBinaryFile;
	private String negWordGraphBinaryFile;
	private ArrayList<String> posTestFilenames;
	private ArrayList<String> negTestFilenames;

	private int minPosRating;
	private int maxPosRating;
	private int minNegRating;
	private int maxNegRating;
	private double posRate;
	private boolean shuffle;
	private long seed; 
	private int window;
	private boolean preprocess;
	private int id;

	private String wekaClassifierName;
	private String classifierName;

	private int noOfWordGraphsReviews;
	private int noTrainReviews;
	private int noTestReviews;
	private int thresholdNoOfAppearances;
	private double thresholdPercentage;
	private String wordGraphsClassifierBinaryFile;

	private File resultsDirectory;
	private String resultsFile;

	public static void main(String[] args) throws Exception {
		if (args.length != 3)
			return;

		InputStream reader = new FileInputStream(args[0]);
		Properties properties = new Properties();

		if (args[0].contains(".xml"))
			properties.loadFromXML(reader);
		else
			properties.load(reader);

		boolean stage3 = Boolean.parseBoolean(args[1]);
		boolean allStages = Boolean.parseBoolean(args[2]);
		
		long start = System.currentTimeMillis();

		System.out.println("Running Third Stage of Sentiment "
				+ "Classification: Creating and evaluating classifier\n");

		Classifiers object = new Classifiers();

		object.setParameters(properties, true);
		object.createResultsDirectory();
		object.setFiles(allStages);
		object.findTestFilenames(stage3);
		System.out.println("Creating classifier...\n");

		long wordGraphs = System.currentTimeMillis();
		object.createClassifierForWordGraphs();

		System.out.println("Evaluating classifier...\n");
		long wordGraphs2 = System.currentTimeMillis();
		object.evaluateClassifierForWordGraphs();

		long end = System.currentTimeMillis();

		NumberFormat formatter = new DecimalFormat("#0.00000");

		FileWriter output = new FileWriter(new File(
				object.getResultsFile()), true);

		output.write("Time for creating classifier: "); 
		output.write(formatter.format((wordGraphs2 - wordGraphs) / 1000d)
				+ " seconds\n");

		output.write("Time for evaluating classifier: "); 
		output.write(formatter.format((end - wordGraphs2) / 1000d)
				+ " seconds\n");

		output.write("\nExecution time is "); 
		output.write(formatter.format((end - start) / 1000d) + " seconds");
		output.close();

		System.out.print("\nExecution time is " + 
				formatter.format((end - start) / 1000d) + " seconds");

	}

	public void setParameters(Properties properties, boolean onlyThisStage) {
		setWekaClassifierName(properties.getProperty("classifierName"));
		String[] classifier = getWekaClassifierName().split("\\.");
		String classifierName = classifier[classifier.length - 1];
		setClassifierName(classifierName);
		setPosTestFilepath(properties.getProperty("positiveTestFilepath"));
		setNegTestFilepath(properties.getProperty("negativeTestFilepath"));
		setTaggerFilepath(properties.getProperty("taggerFilepath"));
		setPOSRulesHashtableFilepath(properties.getProperty("POSRulesHashtableFilepath"));
		setSentiWordNetFilepath(properties.getProperty("SentiWordNetFilepath"));
		setNoTestReviews(Integer.parseInt(properties.getProperty("noOfTestReviews")));
		setPositiveRate(Double.parseDouble(properties.getProperty("positiveRate"))/100);
		setWindow(Integer.parseInt(properties.getProperty("windowSize")));
		if (properties.getProperty("seed") != null) 
			setSeed(Long.parseLong(properties.getProperty("seed")));
		else
			setSeed(-1);

		setShuffle(Boolean.parseBoolean(properties.getProperty("shuffle")));

		setMinPosRating(Integer.parseInt(properties.getProperty("minPositiveRating")));
		setMaxPosRating(Integer.parseInt(properties.getProperty("maxPositiveRating")));
		setMinNegRating(Integer.parseInt(properties.getProperty("minNegativeRating")));
		setMaxNegRating(Integer.parseInt(properties.getProperty("maxNegativeRating")));
		setThresholdNoOfAppearances(Integer.parseInt(properties.getProperty("thresholdNoOfAppearances")));
		setThresholdPercentage(Integer.parseInt(properties.getProperty("thresholdPercentage")));
		
		if (onlyThisStage) {
			System.out.println("Setting parameters...");
			setWordGraphsTrainFile(properties.getProperty("wordGraphsTrainFile"));
			setPosWordGraphBinaryFile(properties.getProperty("posWordGraphBinaryFile"));
			setNegWordGraphBinaryFile(properties.getProperty("negWordGraphBinaryFile"));
		}
	}

	public void createResultsDirectory() {
		System.out.println("Creating directory for results...");
		String results;

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		
		results = getNoTestReviews() + "_" + getClassifierName();
		results+= "_"+ getThresholdPercentage() +"PER";
		results += "_" + day + "" + month + "" + year;
		results += "_" + hours + "." + minutes + "." + seconds;
		setResultsDirectory(new File(results));
		getResultsDirectory().mkdir();
	}

	public void setFiles(boolean allStages) {
		String filePrefix = getResultsDirectory() + "/Binary" 
				+ getClassifierName();

		if (allStages) {
			setWordGraphsClassifierBinaryFile(filePrefix 
					+ "ClassifierWordGraphs");
			setResultsFile(getResultsDirectory() + "/" + "Results.txt");
		} else {

			System.out.println("Setting the output filenames...");
			String fileSuffix = id + "_" + 
					getNoOfWordGraphsReviews() + "_" + 
					getNoTrainReviews() + "_" + getNoTestReviews(); 

			setWordGraphsClassifierBinaryFile(filePrefix + "ClassifierWordGraphs_" 
					+ fileSuffix);
			setResultsFile(getResultsDirectory() + "/" + fileSuffix + "_Results.txt");

		}
	}

	public void findTestFilenames(boolean stage3) throws IOException {
		System.out.println("Selecting reviews for the testing corpus...");
		
		FilenamePattern filePattern = 
				new FilenamePattern(getMinPosRating(), getMaxPosRating());
		setPosTestFilenames(
				filePattern.findFilenames(getPosTestFilepath(), 
						(int) (getNoTestReviews() * posRate), 
						isShuffle(), getSeed()));
		
		filePattern = 
				new FilenamePattern(getMinNegRating(), getMaxNegRating());
		setNegTestFilenames(
				filePattern.findFilenames(getNegTestFilepath(), 
						(int) (getNoTestReviews() * (1 - posRate)), 
						isShuffle(), getSeed()));

		if (stage3) {
			writeFilenames(getPosTestFilenames(), "FilenamesForTest_" + id 
					+ "_" + getNoTestReviews() + ".txt");
			writeFilenames(getNegTestFilenames(), "FilenamesForTest_" + id 
					+ "_" + getNoTestReviews() + ".txt");
		} else {
			writeFilenames(getPosTestFilenames(), "FilenamesForTest.txt");
			writeFilenames(getNegTestFilenames(), "FilenamesForTest.txt");
			}
	}
	
	public void writeFilenames(ArrayList<String> filenames, String outputFile) 
			throws IOException {
		
		FileWriter writer = new FileWriter(
				new File(getResultsDirectory() + "/" + outputFile), true);
		
		for (String s: filenames)
			writer.write(s +  "\n");
		
		writer.close();
	}
	
	public void createClassifierForWordGraphs() throws Exception {

		SentimentClassifier sentimentClassifier = 
				new SentimentClassifier(getWekaClassifierName(), 
						getWordGraphsTrainFile());

		sentimentClassifier.createClassifierInstance();
		sentimentClassifier.trainClassifier();
		sentimentClassifier.storeSentimentClassifier(
				getWordGraphsClassifierBinaryFile());
	}

	public void evaluateClassifierForWordGraphs() throws Exception {

		SentimentAnalysisImp analyser = new SentimentAnalysisImp(); 

		setAnalyserParameters(analyser);
		int counter = 0;
		ArrayList<String> posfiles = getPosTestFilenames();
		ArrayList<String> negfiles = getNegTestFilenames();

		for(String file : posfiles) {
			BufferedReader br = new BufferedReader(new FileReader(getPosTestFilepath() + file));
			String text = br.readLine();
			int sentiment = analyser.findSentiment(text);
			if (sentiment == 1) counter++; 
			br.close();
		}

		for(String file : negfiles) {
			BufferedReader br = new BufferedReader(new FileReader(getNegTestFilepath() + file));
			String text = br.readLine();
			int sentiment = analyser.findSentiment(text);
			if (sentiment == 0) counter++; 
			br.close();
		}
		
		FileWriter writer = new FileWriter ((getResultsDirectory() + "/POSRulesHashtable" 
				+ getThresholdPercentage() + "_" + getThresholdNoOfAppearances() + ".txt"), true);

		analyser.SentimentPOSRulesHashtable.storeHashtable(analyser.SentimentPOSRulesHashtable.getPOSRulesHashtable(), writer);
		writer.close();
		
		FileWriter writer1 = new FileWriter(getResultsFile(), true);
		writer1.write("Success Rate: " + (counter*1.0/getNoTestReviews())*100 + "%\n");
		writer1.close();
		System.out.println("Success Rate: " + (counter*1.0/getNoTestReviews())*100 + "%");

	}

	public void setAnalyserParameters(SentimentAnalysisImp analyser) throws ClassNotFoundException, IOException{
		analyser.setValues(new WordGraphsSimilarities(getPosWordGraphBinaryFile(), getNegWordGraphBinaryFile()));
		SentimentClassifier s_classifier = new SentimentClassifier();
		s_classifier.loadSentimentClassifier(getWordGraphsClassifierBinaryFile());
		analyser.setClassifier(s_classifier.getClassifierInstance());
		analyser.setSentimentPOSRulesHashtable(new POSRulesHashtable(getTaggerFilepath(),getPOSRulesHashtableFilepath(),
				getSentiWordNetFilepath(), getThresholdNoOfAppearances(), getThresholdPercentage()));
		analyser.setWindow(getWindow());
		analyser.setPreprocess(isPreprocess());
	}

	public String getWordGraphsTrainFile() {
		return wordGraphsTrainFile;
	}

	public void setWordGraphsTrainFile(String wordGraphsTrainFile) {
		this.wordGraphsTrainFile = wordGraphsTrainFile;
	}

	public String getWekaClassifierName() {
		return wekaClassifierName;
	}

	public void setWekaClassifierName(String wekaClassifierName) {
		this.wekaClassifierName = wekaClassifierName;
	}

	public String getClassifierName() {
		return classifierName;
	}

	public void setClassifierName(String classifierName) {
		this.classifierName = classifierName;
	}

	public int getNoTrainReviews() {
		return noTrainReviews;
	}

	public void setNoTrainReviews(int noTrainReviews) {
		this.noTrainReviews = noTrainReviews;
	}

	public int getNoTestReviews() {
		return noTestReviews;
	}

	public void setNoTestReviews(int noTestReviews) {
		this.noTestReviews = noTestReviews;
	}


	public int getNoOfWordGraphsReviews() {
		return noOfWordGraphsReviews;
	}

	public void setNoOfWordGraphsReviews(int noOfWordGraphsReviews) {
		this.noOfWordGraphsReviews = noOfWordGraphsReviews;
	}

	public void setId(int id) {
		this.id = id;
	}

	public File getResultsDirectory() {
		return resultsDirectory;
	}

	public void setResultsDirectory(File resultsDirectory) {
		this.resultsDirectory = resultsDirectory;
	}

	public boolean isPreprocess() {
		return preprocess;
	}

	public void setPreprocess(boolean preprocess) {
		this.preprocess = preprocess;
	}

	public String getWordGraphsClassifierBinaryFile() {
		return wordGraphsClassifierBinaryFile;
	}

	public void setWordGraphsClassifierBinaryFile(
			String wordGraphsClassifierBinaryFile) {
		this.wordGraphsClassifierBinaryFile = wordGraphsClassifierBinaryFile;
	}

	public String getResultsFile() {
		return resultsFile;
	}

	public void setResultsFile(String resultsFile) {
		this.resultsFile = resultsFile;
	}

	public int getId() {
		return id;
	}

	public String getPosTestFilepath() {
		return posTestFilepath;
	}

	public void setPosTestFilepath(String posTestFilepath) {
		this.posTestFilepath = posTestFilepath;
	}

	public String getNegTestFilepath() {
		return negTestFilepath;
	}

	public void setNegTestFilepath(String negTestFilepath) {
		this.negTestFilepath = negTestFilepath;
	}

	public ArrayList<String> getPosTestFilenames() {
		return posTestFilenames;
	}

	public void setPosTestFilenames(ArrayList<String> posTestFilenames) {
		this.posTestFilenames = posTestFilenames;
	}

	public ArrayList<String> getNegTestFilenames() {
		return negTestFilenames;
	}

	public void setNegTestFilenames(ArrayList<String> negTestFilenames) {
		this.negTestFilenames = negTestFilenames;
	}

	public int getMinPosRating() {
		return minPosRating;
	}

	public void setMinPosRating(int minPosRating) {
		this.minPosRating = minPosRating;
	}

	public int getMaxPosRating() {
		return maxPosRating;
	}

	public void setMaxPosRating(int maxPosRating) {
		this.maxPosRating = maxPosRating;
	}

	public int getMinNegRating() {
		return minNegRating;
	}

	public void setMinNegRating(int minNegRating) {
		this.minNegRating = minNegRating;
	}

	public int getMaxNegRating() {
		return maxNegRating;
	}

	public void setMaxNegRating(int maxNegRating) {
		this.maxNegRating = maxNegRating;
	}

	public double getPositiveRate() {
		return posRate;
	}

	public void setPositiveRate(double positiveRate) {
		this.posRate = positiveRate;
	}

	public boolean isShuffle() {
		return shuffle;
	}

	public void setShuffle(boolean shuffle) {
		this.shuffle = shuffle;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}

	public String getTaggerFilepath() {
		return taggerFilepath;
	}

	public void setTaggerFilepath(String taggerFilepath) {
		this.taggerFilepath = taggerFilepath;
	}

	public String getPOSRulesHashtableFilepath() {
		return POSRulesHashtableFilepath;
	}

	public void setPOSRulesHashtableFilepath(String POSRulesHashtableFilepath) {
		this.POSRulesHashtableFilepath = POSRulesHashtableFilepath;
	}

	public String getSentiWordNetFilepath() {
		return SentiWordNetFilepath;
	}

	public void setSentiWordNetFilepath(String SentiWordNetFilepath) {
		this.SentiWordNetFilepath = SentiWordNetFilepath;
	}

	public String getPosWordGraphBinaryFile() {
		return posWordGraphBinaryFile;
	}

	public void setPosWordGraphBinaryFile(String posWordGraphBinaryFile) {
		this.posWordGraphBinaryFile = posWordGraphBinaryFile;
	}

	public String getNegWordGraphBinaryFile() {
		return negWordGraphBinaryFile;
	}

	public void setNegWordGraphBinaryFile(String negWordGraphBinaryFile) {
		this.negWordGraphBinaryFile = negWordGraphBinaryFile;
	}

	public int getWindow() {
		return window;
	}

	public void setWindow(int window) {
		this.window = window;
	}

	public int getThresholdNoOfAppearances() {
		return thresholdNoOfAppearances;
	}

	public void setThresholdNoOfAppearances(int thresholdNoOfAppearances) {
		this.thresholdNoOfAppearances = thresholdNoOfAppearances;
	}

	public double getThresholdPercentage() {
		return thresholdPercentage;
	}

	public void setThresholdPercentage(double thresholdPercentage) {
		this.thresholdPercentage = thresholdPercentage;
	}

}

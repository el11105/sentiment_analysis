/**
 * 
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import weka.classifiers.*;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class SentimentClassifier {

	private String classifierName;
	private String trainFile;
	private String filter;
	private Classifier classifier;
	private FilteredClassifier filteredClassifier;

	public SentimentClassifier() { }

	public SentimentClassifier(String classifierName, String trainFile) {
		this.classifierName = classifierName;
		this.trainFile = trainFile;
		filter = null;
	}

	public SentimentClassifier(String classifierName, String trainFile, 
			String filter) {
		this.classifierName = classifierName;
		this.trainFile = trainFile;
		this.filter = filter;
	}

	public void createClassifierInstance() 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException {
		classifier = (Classifier) Class.forName(classifierName).newInstance();
	}

	public void trainClassifier() throws Exception {
		if (filter == null) {
			Instances trainInstances = null;
			if (classifierName.equals(
					"weka.classifiers.bayes.NaiveBayesMultinomial")) {
				trainInstances = discretizeSimilarityValues(trainFile);
			} else {
				BufferedReader reader = 
						new BufferedReader(new FileReader(trainFile));
				trainInstances = new Instances(reader);
			}
			trainInstances.setClassIndex(trainInstances.numAttributes() - 1);
			classifier.buildClassifier(trainInstances);
		}
	}
	

	public void trainClassifier(boolean preprocess) throws Exception {
		if (filter == "StringToWordVector") {
			BufferedReader reader = new BufferedReader(
					new FileReader(trainFile));
			Instances trainInstances = new Instances(reader);
			trainInstances.setClassIndex(trainInstances.numAttributes() - 1);
			stringToWordVector(preprocess);
			filteredClassifier.buildClassifier(trainInstances);
		} 
	}
	
	private void stringToWordVector(boolean preprocess) {
		filteredClassifier = new FilteredClassifier();
		filteredClassifier.setClassifier(this.classifier);
		
		StringToWordVector filter = new StringToWordVector();
		filter.setAttributeIndices("1,2");
		filter.setWordsToKeep(100000);
		filter.setLowerCaseTokens(true);
		
		if (!preprocess) {
			WordTokenizer tokenizer = new WordTokenizer();
			tokenizer.setDelimiters(" ");
			filter.setTokenizer(tokenizer);
			filter.setLowerCaseTokens(false);
		}
		filter.setOutputWordCounts(true);
		filteredClassifier.setFilter(filter);
	}
	
	public Instances discretizeSimilarityValues(String file) 
			throws IOException {

		AttributeRelationFile discreteValuesFile = 
				new AttributeRelationFile("Sentiment_of_Discrete_"
						+ "Similarities_of_ImdbReviewGraphs");
		
		discreteValuesFile.createFile(file);
		String foldername = new File(file).getParent();
		String filename = new File(file).getName();
		String prefix = filename.split("\\.")[0];
		String newFile = foldername + "/" + prefix + "_discretized.arff";
		discreteValuesFile.storeToFile(newFile);
		if (prefix.contains("Train"))
			trainFile = newFile;
		return discreteValuesFile.getInstances();
	}

	public void storeSentimentClassifier(String outputClassifierFile) 
			throws IOException {
		
		FileOutputStream output = new FileOutputStream(outputClassifierFile);
		ObjectOutputStream outputClassifierStream = 
				new ObjectOutputStream(output);
		
		outputClassifierStream.writeObject(classifierName);
		outputClassifierStream.writeObject(trainFile);
		outputClassifierStream.writeObject(filter);
		outputClassifierStream.writeObject(classifier);
		outputClassifierStream.writeObject(filteredClassifier);
		outputClassifierStream.close();
	}

	public void loadSentimentClassifier(String inputClassifierFile) 
			throws IOException, ClassNotFoundException {
		
		FileInputStream input = new FileInputStream(inputClassifierFile);
		ObjectInputStream inputClassifierStream = new ObjectInputStream(input);
		
		classifierName = (String) inputClassifierStream.readObject();
		trainFile = (String) inputClassifierStream.readObject();
		filter = (String) inputClassifierStream.readObject();
		classifier = (Classifier) inputClassifierStream.readObject();
		filteredClassifier = 
				(FilteredClassifier) inputClassifierStream.readObject();
		inputClassifierStream.close();
	}
	
	public void loadSentimentClassifier(InputStream input) 
			throws IOException, ClassNotFoundException {
		
		//FileInputStream input = new FileInputStream(inputClassifierFile);
		ObjectInputStream inputClassifierStream = new ObjectInputStream(input);
		
		classifierName = (String) inputClassifierStream.readObject();
		trainFile = (String) inputClassifierStream.readObject();
		filter = (String) inputClassifierStream.readObject();
		classifier = (Classifier) inputClassifierStream.readObject();
		filteredClassifier = 
				(FilteredClassifier) inputClassifierStream.readObject();
		inputClassifierStream.close();
	}

	public String getClassifierName() {
		return classifierName;
	}
	
	public String getTrainFile() {
		return trainFile;
	}
	public void setTrainFile(String trainFile) {
		this.trainFile = trainFile;
	}
	
	public String getFilter() {
		return filter;
	}
	
	public Classifier getClassifierInstance() {
		return classifier;
	}
	
	public FilteredClassifier getFilteredClassifierInstance() {
		return filteredClassifier;
	}
}

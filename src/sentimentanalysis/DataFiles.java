/**
 * 
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class DataFiles {
	
	private int noTrainReviews;
	private int noTestReviews;
	
	private String wordGraphsTrainFile;
	//private String wordGraphsTestFile;
	
	private String posTrainFilepath;
	private String negTrainFilepath;
	
	private String posTestFilepath;
	private String negTestFilepath;
	private String taggerFilepath;
	private String POSRulesHashtableFilepath;
	private String SentiWordNetFilepath;
	private int thresholdNoOfAppearances;
	private double thresholdPercentage;
	
	private ArrayList<String> posGraphFilenames;
	private ArrayList<String> negGraphFilenames;
	private ArrayList<String> posTrainFilenames;
	private ArrayList<String> negTrainFilenames;
	private ArrayList<String> posTestFilenames;
	private ArrayList<String> negTestFilenames;
	
	private double posRate;
	private boolean shuffle;
	private long seed; 
	
	private int minPosRating;
	private int maxPosRating;
	private int minNegRating;
	private int maxNegRating;

	private String posWordGraphBinaryFile;
	private String negWordGraphBinaryFile;
	
	private int id;

	private File resultsDirectory;
	
	public static void main(String[] args) 
			throws IOException, ClassNotFoundException {
		
		if (args.length != 3)
			return;
		
		InputStream reader = new FileInputStream(args[0]);
		Properties properties = new Properties();
		
		if (args[0].contains(".xml"))
			properties.loadFromXML(reader);
		else
			properties.load(reader);
		
		boolean stage2 = Boolean.parseBoolean(args[1]);
		boolean allStages = Boolean.parseBoolean(args[2]);
		
		long start = System.currentTimeMillis();
		
		System.out.println("Running Second Stage of Sentiment "
				+ "Classification: Creating training and testing files for"
				+ " each approach\n");
		
		DataFiles object = new DataFiles();
		
		object.setParameters(properties, true);
		object.createResultsDirectory();
		object.setFiles(allStages);
		
		object.findTrainFilenames(stage2);
		//object.findTestFilenames(stage2);
		
		long wordGraphs = System.currentTimeMillis();
		object.wordGraphsFilesARFF();
		long end = System.currentTimeMillis();
		
		NumberFormat formatter = new DecimalFormat("#0.00000");
		String info = object.getNoTrainReviews() + "_" + object.getNoTestReviews();
		
		FileWriter output = 
				new FileWriter(new File(object.getResultsDirectory() 
						+ "/" + info + "_Info.txt"));
		
		output.write("Time for files of word graphs: "); 
		output.write(formatter.format((end - wordGraphs) / 1000d) + " seconds\n");
		output.write("\nExecution time is "); 
		output.write(formatter.format((end - start) / 1000d) + " seconds");
		output.close();
		
		System.out.print("\nExecution time is " + formatter.format((end - start) / 1000d) + " seconds");
		
		return;
	}
	
	public void setParameters(Properties properties, boolean onlyThisStage) 
			throws ClassNotFoundException, IOException {
		
		setNoTrainReviews(Integer.parseInt(
				properties.getProperty("noOfTrainReviews")));
		setNoTestReviews(Integer.parseInt(
				properties.getProperty("noOfTestReviews")));
		
		setPosTrainFilepath(properties.getProperty("positiveTrainFilepath"));
		setNegTrainFilepath(properties.getProperty("negativeTrainFilepath"));
		setPosTestFilepath(properties.getProperty("positiveTestFilepath"));
		setNegTestFilepath(properties.getProperty("negativeTestFilepath"));
		setTaggerFilepath(properties.getProperty("taggerFilepath"));
		setPOSRulesHashtableFilepath(properties.getProperty("POSRulesHashtableFilepath")); //put binary file 
		setSentiWordNetFilepath(properties.getProperty("SentiWordNetFilepath"));
		setThresholdNoOfAppearances(Integer.parseInt(properties.getProperty("thresholdNoOfAppearances")));
		setThresholdPercentage(Integer.parseInt(properties.getProperty("thresholdPercentage")));
		setMinPosRating(Integer.parseInt(
				properties.getProperty("minPositiveRating")));
		
		setMaxPosRating(Integer.parseInt(
				properties.getProperty("maxPositiveRating")));
		
		setMinNegRating(Integer.parseInt(
				properties.getProperty("minNegativeRating")));
		
		setMaxNegRating(Integer.parseInt(
				properties.getProperty("maxNegativeRating")));
		
		setPosRate(Double.parseDouble(
				properties.getProperty("positiveRate"))/100);
		
		if (properties.getProperty("seed") != null) 
			setSeed(Long.parseLong(properties.getProperty("seed")));
		else
			setSeed(-1);

		setShuffle(Boolean.parseBoolean(
				properties.getProperty("shuffle")));

		if (onlyThisStage) {
			System.out.println("Setting parameters...");
			setPosWordGraphBinaryFile(
					properties.getProperty("posWordGraphFilepath"));
			
			setNegWordGraphBinaryFile(
					properties.getProperty("negWordGraphFilepath"));
			setPosWordGraphFilenames(getPosWordGraphBinaryFile());
			setNegWordGraphFilenames(getNegWordGraphBinaryFile());
		}
	}
	
	public void createResultsDirectory() {
		
		System.out.println("Creating directory for results...");
		String results;

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		
		results = getNoTrainReviews() + "_" + getNoTestReviews();
		results += "_" + day + "" + month + "" + year;
		results += "_" + hours + "." + minutes + "." + seconds;
		results+= "_Train&TestFiles";
		
		setResultsDirectory(new File(results));
		getResultsDirectory().mkdir();
	}

public void setFiles(boolean allStages) {
		
		String filePrefix = getResultsDirectory() + "/";
		//String fileInfo = "_" + getNoOfWordGraphsReviews() + "_";
		id = 0;
		if (allStages) {
			setWordGraphsTrainFile(filePrefix + "Train" + getNoTrainReviews() 
					+ "WordGraph.arff");
		} else {
			System.out.println("Setting the output filenames...");
			File filepath = getResultsDirectory();
			File[] allFiles = filepath.listFiles();
			if (allFiles != null)
				id = findIdOfLastFile(allFiles);
			
			String fileTrainSuffix = getNoTrainReviews() + "_TrainWordGraph.arff";
			//String fileTestSuffix = getNoTestReviews() + "_TestWordGraph.arff";
			setWordGraphsTrainFile(filePrefix + id + fileTrainSuffix);
		}
	}
	
	private int findIdOfLastFile(File[] allFiles) {
		long lastMod = Long.MIN_VALUE;
		File last = null;
		for (File file : allFiles)
			if (file.lastModified() > lastMod) {
				last = file;
				lastMod = file.lastModified();
			}
			
		if (last != null)
			return Integer.parseInt(last.getName().split("_")[0]) + 1;
		else
			return 0;
	}
	
	public void findTrainFilenames(boolean stage2) throws IOException {
		System.out.println("Selecting reviews for the training corpus...");

		FilenamePattern filePattern = 
				new FilenamePattern(getMinPosRating(), getMaxPosRating());
		
		setPosTrainFilenames(
				filePattern.findFilenames(getPosTrainFilepath(), 
						(int) (getNoTrainReviews() * getPosRate()), 
						getPosGraphFilenames(), isShuffle(), getSeed()));
		
		filePattern = 
				new FilenamePattern(getMinNegRating(), getMaxNegRating());
		
		setNegTrainFilenames(
				filePattern.findFilenames(getNegTrainFilepath(), 
						(int) (getNoTrainReviews() * (1 - getPosRate())), 
						getNegGraphFilenames(), isShuffle(), getSeed()));
		
		if (stage2) {
			writeFilenames(getPosTrainFilenames(), "FilenamesForTrain_" + id 
					+ "_" + getNoTrainReviews() + ".txt");
			writeFilenames(getNegTrainFilenames(), "FilenamesForTrain_" + id 
					+ "_" + getNoTrainReviews() + ".txt");
		} else {
			writeFilenames(getPosTrainFilenames(), "FilenamesForTrain.txt");
			writeFilenames(getNegTrainFilenames(), "FilenamesForTrain.txt");
		}
	}
	
	public void writeFilenames(ArrayList<String> filenames, String outputFile) 
			throws IOException {
		
		FileWriter writer = new FileWriter(
				new File(getResultsDirectory() + "/" + outputFile), true);
		
		for (String s: filenames)
			writer.write(s +  "\n");
		
		writer.close();
	}
	
	public void wordGraphsFilesARFF() 
			throws ClassNotFoundException, IOException {
		
		System.out.println("Creating training and testing files by comparing "
				+ "reviews to model word graphs...");
		
		WordGraphsSimilarities values = 
				new WordGraphsSimilarities(getPosWordGraphBinaryFile(), 
						getNegWordGraphBinaryFile());

		POSRulesHashtable SentimentPOSRules = new POSRulesHashtable(getTaggerFilepath(), 
				getPOSRulesHashtableFilepath(), getSentiWordNetFilepath(), getThresholdNoOfAppearances(), getThresholdPercentage()); 
		
		AttributeRelationFile train = createTrainFileARFF(values, SentimentPOSRules);
		train.storeToFile(getWordGraphsTrainFile());
		train = null;
		/*
		AttributeRelationFile test = createTestFileARFF(values, SentimentPOSRules);
		test.storeToFile(getWordGraphsTestFile());
		*/
	}
	
	public AttributeRelationFile createTrainFileARFF(WordGraphsSimilarities values, POSRulesHashtable SentimentPOSRules) 
			throws FileNotFoundException, IOException, ClassNotFoundException {
		
		AttributeRelationFile file = 
				new AttributeRelationFile("Sentiment_of_Similarities_of_"
						+ "ImdbReviewWordGraphs");
		
		file.createFile(values, SentimentPOSRules, getPosTrainFilepath(), getNegTrainFilepath(), 
				getPosTrainFilenames(), getNegTrainFilenames());
		
		return file;
	}
/*
	public AttributeRelationFile createTestFileARFF(WordGraphsSimilarities values, SentimentPOSRulesHashtable SentimentPOSRules) 
			throws FileNotFoundException, IOException, ClassNotFoundException {
		
		AttributeRelationFile file = 
				new AttributeRelationFile("Sentiment_of_Similarities_of_"
						+ "ImdbReviewWordGraphs");

		file.createFile(values, SentimentPOSRules, getPosTestFilepath(), getNegTestFilepath(), 
				getPosTestFilenames(), getNegTestFilenames());
		
		return file;
	}	
*/
	public int getNoTrainReviews() {
		return noTrainReviews;
	}
	
	public void setNoTrainReviews(int noTrainReviews) {
		this.noTrainReviews = noTrainReviews;
	}
	
	public int getNoTestReviews() {
		return noTestReviews;
	}
	
	public void setNoTestReviews(int noTestReviews) {
		this.noTestReviews = noTestReviews;
	}
	
	public String getPosTrainFilepath() {
		return posTrainFilepath;
	}

	public void setPosTrainFilepath(String posTrainFilepath) {
		this.posTrainFilepath = posTrainFilepath;
	}

	public String getNegTrainFilepath() {
		return negTrainFilepath;
	}

	public void setNegTrainFilepath(String negTrainFilepath) {
		this.negTrainFilepath = negTrainFilepath;
	}

	public String getPosTestFilepath() {
		return posTestFilepath;
	}

	public void setPosTestFilepath(String posTestFilepath) {
		this.posTestFilepath = posTestFilepath;
	}

	public String getNegTestFilepath() {
		return negTestFilepath;
	}

	public void setNegTestFilepath(String negTestFilepath) {
		this.negTestFilepath = negTestFilepath;
	}

	public String getTaggerFilepath() {
		return taggerFilepath;
	}

	public void setTaggerFilepath(String taggerFilepath) {
		this.taggerFilepath = taggerFilepath;
	}

	public String getPOSRulesHashtableFilepath() {
		return POSRulesHashtableFilepath;
	}

	public void setPOSRulesHashtableFilepath(String POSRulesHashtableFilepath) {
		this.POSRulesHashtableFilepath = POSRulesHashtableFilepath;
	}

	public String getSentiWordNetFilepath() {
		return SentiWordNetFilepath;
	}

	public void setSentiWordNetFilepath(String sentiWordNetFilepath) {
		this.SentiWordNetFilepath = sentiWordNetFilepath;
	}

	public double getPosRate() {
		return posRate;
	}
	
	public void setPosRate(double posRate) {
		this.posRate = posRate;
	}

	public boolean isShuffle() {
		return shuffle;
	}

	public void setShuffle(boolean shuffle) {
		this.shuffle = shuffle;
	}

	public int getMinPosRating() {
		return minPosRating;
	}

	public void setMinPosRating(int minPosRating) {
		this.minPosRating = minPosRating;
	}

	public int getMaxPosRating() {
		return maxPosRating;
	}

	public void setMaxPosRating(int maxPosRating) {
		this.maxPosRating = maxPosRating;
	}

	public int getMinNegRating() {
		return minNegRating;
	}

	public void setMinNegRating(int minNegRating) {
		this.minNegRating = minNegRating;
	}

	public int getMaxNegRating() {
		return maxNegRating;
	}

	public void setMaxNegRating(int maxNegRating) {
		this.maxNegRating = maxNegRating;
	}

	public String getPosWordGraphBinaryFile() {
		return posWordGraphBinaryFile;
	}

	public void setPosWordGraphBinaryFile(String posWordGraphBinaryFile) {
		this.posWordGraphBinaryFile = posWordGraphBinaryFile;
	}

	public String getNegWordGraphBinaryFile() {
		return negWordGraphBinaryFile;
	}

	public void setNegWordGraphBinaryFile(String negWordGraphBinaryFile) {
		this.negWordGraphBinaryFile = negWordGraphBinaryFile;
	}
	
	public void setPosWordGraphFilenames(String posWordGraphFilepath) 
			throws ClassNotFoundException, IOException {
	
		ModelWordGraph model = new ModelWordGraph();
		model.loadModelGraph(posWordGraphFilepath);
		
		setPosGraphFilenames(model.getReviewFilenames());
	}
	
	public void setNegWordGraphFilenames(String negWordGraphFilepath) 
			throws ClassNotFoundException, IOException {
		
		ModelWordGraph model = new ModelWordGraph();
		model.loadModelGraph(negWordGraphFilepath);
		
		setNegGraphFilenames(model.getReviewFilenames());
	}

	public ArrayList<String> getPosGraphFilenames() {
		return posGraphFilenames;
	}

	public void setPosGraphFilenames(ArrayList<String> posGraphFilenames) {
		this.posGraphFilenames = posGraphFilenames;
	}

	public ArrayList<String> getNegGraphFilenames() {
		return negGraphFilenames;
	}

	public void setNegGraphFilenames(ArrayList<String> negGraphFilenames) {
		this.negGraphFilenames = negGraphFilenames;
	}
	

	public File getResultsDirectory() {
		return resultsDirectory;
	}

	public void setResultsDirectory(File resultsDirectory) {
		this.resultsDirectory = resultsDirectory;
	}
	
	public String getWordGraphsTrainFile() {
		return wordGraphsTrainFile;
	}

	public void setWordGraphsTrainFile(String wordGraphsTrainFile) {
		this.wordGraphsTrainFile = wordGraphsTrainFile;
	}


	
	public ArrayList<String> getPosTrainFilenames() {
		return posTrainFilenames;
	}

	public void setPosTrainFilenames(ArrayList<String> posTrainFilenames) {
		this.posTrainFilenames = posTrainFilenames;
	}

	public ArrayList<String> getNegTrainFilenames() {
		return negTrainFilenames;
	}

	public void setNegTrainFilenames(ArrayList<String> negTrainFilenames) {
		this.negTrainFilenames = negTrainFilenames;
	}

	public ArrayList<String> getPosTestFilenames() {
		return posTestFilenames;
	}

	public void setPosTestFilenames(ArrayList<String> posTestFilenames) {
		this.posTestFilenames = posTestFilenames;
	}

	public ArrayList<String> getNegTestFilenames() {
		return negTestFilenames;
	}

	public void setNegTestFilenames(ArrayList<String> negTestFilenames) {
		this.negTestFilenames = negTestFilenames;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}

	public int getThresholdNoOfAppearances() {
		return thresholdNoOfAppearances;
	}

	public void setThresholdNoOfAppearances(int thresholdNoOfAppearances) {
		this.thresholdNoOfAppearances = thresholdNoOfAppearances;
	}

	public double getThresholdPercentage() {
		return thresholdPercentage;
	}

	public void setThresholdPercentage(double thresholdPercentage) {
		this.thresholdPercentage = thresholdPercentage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}

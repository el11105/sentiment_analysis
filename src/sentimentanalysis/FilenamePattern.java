/**
 * 
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;


public class FilenamePattern {

	private String patternForMatching;
	
	public FilenamePattern(int minRating, int maxRating) {
		
		patternForMatching = "(^\\d{1,5}[_](";
		
		for (int rating = minRating; rating < maxRating + 1; rating++)
			patternForMatching += rating +"|";
		
		patternForMatching = patternForMatching.substring(0, 
				patternForMatching.length()-1) + ").txt$)";	
	}
	
	public ArrayList<String> findFilenames(String reviewFilepath, 
			int noOfReviews, boolean shuffle) {
		
		File filepath = new File(reviewFilepath);
		String[] allFiles = filepath.list();

		if (shuffle)
			allFiles = shuffle(allFiles);

		ArrayList<String> toFillArray = new ArrayList<String>();
		int size = 0;
		for (String s: allFiles)
			if (match(s)) {
				toFillArray.add(s);
				if (++size >= noOfReviews)
					break;
			}
		return toFillArray;
	}

	public ArrayList<String> findFilenames(String reviewFilepath, 
			int noOfReviews, boolean shuffle, long randomSeed) {
		
		if (randomSeed == -1)
			return findFilenames(reviewFilepath, noOfReviews, shuffle);
		
		File filepath = new File(reviewFilepath);
		String[] allFiles = filepath.list();

		if (shuffle)
			allFiles = shuffle(allFiles, randomSeed);

		ArrayList<String> toFillArray = new ArrayList<String>();
		int size = 0;
		for (String s: allFiles)
			if (match(s)) {
				toFillArray.add(s);
				if (++size >= noOfReviews)
					break;
			}
		return toFillArray;
	}
	
	public ArrayList<String> findFilenames(String filepath, int noOfReviews, 
			ArrayList<String> notMatchingArray, boolean shuffle) {
		
		File reviewFilepath = new File(filepath);
		String[] allFiles = reviewFilepath.list();
		ArrayList<String> toFillArray = new ArrayList<String>();
		int size = 0;
		
		if (shuffle)
			allFiles = shuffle(allFiles);
		
		for (String s: allFiles)
			if ((notMatchingArray!=null & (!notMatchingArray.contains(s))) && match(s)) {
				toFillArray.add(s);
				if (++size >= noOfReviews)
					break;
			}
		return toFillArray;
	}

	public ArrayList<String> findFilenames(String filepath, int noOfReviews, 
			ArrayList<String> notMatchingArray, boolean shuffle, 
			long randomSeed) {

		if (randomSeed == -1)
			return findFilenames(filepath, noOfReviews,	
					notMatchingArray, shuffle);
		
		File reviewFilepath = new File(filepath);
		String[] allFiles = reviewFilepath.list();
		ArrayList<String> toFillArray = new ArrayList<String>();
		int size = 0;
		
		if (shuffle)
			allFiles = shuffle(allFiles, randomSeed);
			
		for (String s: allFiles)
			if ((!notMatchingArray.contains(s)) && match(s)) {
				toFillArray.add(s);
				if (++size >= noOfReviews)
					break;
			}
		return toFillArray;
	}

	private String[] shuffle(String[] filenames) {
		Random randomIndex = new Random();		
		
		for (int i = 0; i < filenames.length; i++) {
		    int randomPosition = randomIndex.nextInt(filenames.length);
		    String temp = filenames[i];
		    filenames[i] = filenames[randomPosition];
		    filenames[randomPosition] = temp;
		}
		return filenames;
	}

	private String[] shuffle(String[] filenames, long randomSeed) {
		Random randomIndex = new Random();
		randomIndex.setSeed(randomSeed);
		
		for (int i = 0; i < filenames.length; i++) {
		    int randomPosition = randomIndex.nextInt(filenames.length);
		    String temp = filenames[i];
		    filenames[i] = filenames[randomPosition];
		    filenames[randomPosition] = temp;
		}
		return filenames;
	}

	private boolean match(String reviewFilename) {
		if (reviewFilename.matches(patternForMatching))
			return true;
		else 
			return false;
	}

	public String getPatternForMatching() {
		return patternForMatching;
	}
}

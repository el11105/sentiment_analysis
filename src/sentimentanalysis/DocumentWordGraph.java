/**
 * 
 * 
 *
 * 
 * -------------------------------------------------------
 * 
 * Copyright (C) 2017 Anastasios Alexopoulos
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * 
 * 
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

package sentimentanalysis;

import gr.demokritos.iit.jinsect.documentModel.representations.DocumentNGramGraph;
import gr.demokritos.iit.jinsect.structs.*;

import java.util.*;

import salvo.jesus.graph.*;

public class DocumentWordGraph extends DocumentNGramGraph {

	private static final long serialVersionUID = 1L;

	public DocumentWordGraph(int windowSize) {
		super(windowSize, windowSize, windowSize);
	}

	public void createGraphs() {

		String sUsableString = (new StringBuilder()).
				append(DataString).toString();

		if(TextPreprocessor != null)
			sUsableString = TextPreprocessor.preprocess(sUsableString);
		
		String[] extractedWords = sUsableString.split("\\s+");
		int length = extractedWords.length;
		String sCurNGram = null;
		HashMap<String, Double> hTokenAppearence = new HashMap<String, Double>();
		
		for (int iCur = 0; iCur < length; iCur++) {
			sCurNGram = extractedWords[iCur];
			
			if(hTokenAppearence.containsKey(sCurNGram))
				hTokenAppearence.put(sCurNGram, 
					Double.valueOf(
					hTokenAppearence.get(sCurNGram).doubleValue() 
					+ 1.0D));
			else
				hTokenAppearence.put(sCurNGram, Double.valueOf(1.0D));
		}
		
		Vector<String> PrecedingNeighbours = new Vector<String>();
		UniqueVertexGraph gGraph = getGraphLevelByNGramSize(MinSize);
		sCurNGram = "";
		
		for (int iCur = 0; iCur < length; iCur++) {
			sCurNGram = extractedWords[iCur];

			if(WordEvaluator != null && !WordEvaluator.evaluateWord(sCurNGram))
				continue;
			
			String aFinalNeighbours[];
			if(Normalizer != null)
				aFinalNeighbours = (String[]) Normalizer.normalize(null, 
						PrecedingNeighbours.toArray());
			else {
				aFinalNeighbours = new String[PrecedingNeighbours.size()];
				PrecedingNeighbours.toArray(aFinalNeighbours);
			}
			
			createEdgesConnecting(gGraph, sCurNGram, 
					Arrays.asList(aFinalNeighbours), hTokenAppearence); 
			
			PrecedingNeighbours.add(sCurNGram);
            
			if(PrecedingNeighbours.size() > CorrelationWindow)
				PrecedingNeighbours.removeElementAt(0);
		}
		
		int iNeighboursLen = PrecedingNeighbours.size();
		if(iNeighboursLen < CorrelationWindow && iNeighboursLen > 0)
			createEdgesConnecting(gGraph, sCurNGram, 
					PrecedingNeighbours, hTokenAppearence);
	}

	public void mergeGraph(DocumentNGramGraph dgOtherGraph, 
			double fWeightPercent) {
		
		if(dgOtherGraph == this)
			return;

		for(int iCurLvl = MinSize; iCurLvl <= MaxSize; iCurLvl++) {
			UniqueVertexGraph gGraph = getGraphLevelByNGramSize(MinSize);
			UniqueVertexGraph gOtherGraph = 
					dgOtherGraph.getGraphLevelByNGramSize(MinSize);
			if(gOtherGraph == null)
				return;
			
			Iterator<?> iIter = gOtherGraph.getEdgeSet().iterator();
			ArrayList<String> lOtherNodes = new ArrayList<String>();
			String sHead;
			double dWeight;
			for(; iIter.hasNext();
					createWeightedEdgesConnecting(gGraph, sHead,
							lOtherNodes, dWeight, dWeight,
							fWeightPercent)) {
				
				WeightedEdge weCurItem = (WeightedEdge)iIter.next();
				sHead = weCurItem.getVertexA().getLabel();
				String sTail = weCurItem.getVertexB().getLabel();
				dWeight = weCurItem.getWeight();
				lOtherNodes.clear();
				lOtherNodes.add(sTail);
			}
		}
	}

	public DocumentWordGraph intersectGraph(DocumentWordGraph dgOtherGraph) {
		
		DocumentWordGraph gRes = new DocumentWordGraph(CorrelationWindow);
		EdgeCachedLocator ecl = new EdgeCachedLocator(1000);
label0:	
		for(int iCurLvl = MinSize; iCurLvl <= MaxSize; iCurLvl++) {
			UniqueVertexGraph gGraph = getGraphLevelByNGramSize(iCurLvl);
			UniqueVertexGraph gOtherGraph = 
					dgOtherGraph.getGraphLevelByNGramSize(iCurLvl);
			UniqueVertexGraph gNewGraph =
					gRes.getGraphLevelByNGramSize(iCurLvl);

			if(gOtherGraph == null)
				continue;
			
			Iterator<?> iIter = gOtherGraph.getEdgeSet().iterator();
			do {
				WeightedEdge weCurItem;
				String sHead;
				String sTail;
				WeightedEdge eEdge;
				do {
					if(!iIter.hasNext())
						continue label0;
					weCurItem = (WeightedEdge)iIter.next();
					sHead = weCurItem.getVertexA().getLabel();
					sTail = weCurItem.getVertexB().getLabel();
					eEdge = (WeightedEdge)ecl.locateEdgeInGraph(gGraph, 
							weCurItem.getVertexA(), weCurItem.getVertexB());
				} while(eEdge == null);

				try {
					List<String> l = new ArrayList<String>();
					l.add(sTail);
					double dTargetWeight = 0.5D * (eEdge.getWeight() + 
							weCurItem.getWeight());
					createWeightedEdgesConnecting(gNewGraph, sHead, l, 
							dTargetWeight, dTargetWeight, 1.0D);
				} catch(Exception e) {
					e.printStackTrace();
				}
			} while(true);
		}
		
		return gRes;
	}

	public DocumentWordGraph allNotIn(DocumentWordGraph dgOtherGraph) {
		
		EdgeCachedLocator eclLocator = 
				new EdgeCachedLocator(Math.max(length(), 
						dgOtherGraph.length()));
		DocumentWordGraph dgClone = (DocumentWordGraph)clone();
label0:	
		for(int iCurLvl = MinSize; iCurLvl <= MaxSize; iCurLvl++) {
			
			UniqueVertexGraph gCloneLevel = 
					dgClone.getGraphLevelByNGramSize(iCurLvl);
			UniqueVertexGraph gOtherGraphLevel = 
					dgOtherGraph.getGraphLevelByNGramSize(iCurLvl);
			
			if(gOtherGraphLevel == null)
				continue;
			
			Iterator<Object> iIter = 
					Arrays.asList(
							gCloneLevel.getEdgeSet().toArray()).iterator();
			do {
				WeightedEdge weCurItem;
				Edge eEdge;
				do {
					if(!iIter.hasNext())
						continue label0;
					weCurItem = (WeightedEdge)iIter.next();
					eEdge = 
						eclLocator.locateDirectedEdgeInGraph(
								gOtherGraphLevel,
								weCurItem.getVertexA(), 
								weCurItem.getVertexB());
				} while(eEdge == null);
				
				try {
					gCloneLevel.removeEdge(weCurItem);
					eclLocator.resetCache();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			} while(true);
		}
		
		return dgClone;
	}

	@SuppressWarnings("unchecked")
	public Object clone() {
		DocumentWordGraph gRes = new DocumentWordGraph(CorrelationWindow);
		gRes.DataString = DataString;
		gRes.DegradedEdges.putAll((HashMap<?, ?>)DegradedEdges.clone());
		gRes.NGramGraphArray = new UniqueVertexGraph[NGramGraphArray.length];
		int iCnt = 0;
		UniqueVertexGraph auniquevertexgraph[] = NGramGraphArray;
		
        int i = auniquevertexgraph.length;
        for(int j = 0; j < i; j++) {
        	UniqueVertexGraph uCur = auniquevertexgraph[j];
        	gRes.NGramGraphArray[iCnt++] = (UniqueVertexGraph)uCur.clone();
        }
        
        gRes.Normalizer = Normalizer;
        gRes.TextPreprocessor = TextPreprocessor;
        gRes.WordEvaluator = WordEvaluator;
        return gRes;
    }
}